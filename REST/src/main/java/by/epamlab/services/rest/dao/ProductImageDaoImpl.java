package by.epamlab.services.rest.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.ProductImage;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.interfaces.IDaoProductImage;

@Repository
public class ProductImageDaoImpl implements IDaoProductImage {

	private static final Logger log = LoggerFactory.getLogger(ProductImageDaoImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void add(ProductImage productImage) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(productImage);
	}

	@Override
	public void add(ProductImage image, Product product) {
		Session session = sessionFactory.getCurrentSession();
		image.setProduct(product);
		session.persist(image);
	}

	@Override
	public ProductImage getByID(long id) {
		Session session = sessionFactory.getCurrentSession();
		ProductImage findProductImage = session.load(ProductImage.class, id);
		if (findProductImage != null) {
			log.info("ProductImage getByID(" + id + "): " + findProductImage);
		} else {
			log.info("ProductImage getByID(" + id + ")  NOT FOUND");
		}
		return findProductImage;

	}

	@Override
	public void update(ProductImage productImage) {
		Session session = sessionFactory.getCurrentSession();
		ProductImage findProductImage = getByID(productImage.getId());
		if (findProductImage != null) {
			session.update(productImage);
			log.info("ProductImage update: \n\tOLD=> " + findProductImage + "\n\tNEW=>" + productImage);
		} else {
			session.persist(productImage);
			log.info("ProductImage NOT FOUND for update... Created new ProductImage: " + productImage);
		}
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		ProductImage findProductImage = getByID(id);
		if (findProductImage != null) {
			session.delete(findProductImage);
			log.info("ProductImage deleted: " + findProductImage);
		} else {
			log.info("ProductImage NOT FOUND for delete... " + findProductImage);
		}
	}

	@Override
	public List<ProductImage> list() {
		Session session = sessionFactory.getCurrentSession();
		List<ProductImage> list = (List<ProductImage>) session.createQuery("from ProductImage").list();
		if (!list.isEmpty()) {
			log.info("listProductImage: \n\t" + list);
		} else {
			log.info("listProductImage: \n\t" + list);
		}
		return list;
	}

	@Override
	public void add(List<ProductImage> list) {
	}

	@Override
	public List<ProductImage> list(DataFilter<ProductImage> filter) {
		return null;
	}

}
