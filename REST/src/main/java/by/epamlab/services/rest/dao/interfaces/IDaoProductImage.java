package by.epamlab.services.rest.dao.interfaces;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.ProductImage;

public interface IDaoProductImage extends IDaoCRUID<ProductImage> {
	public void add(ProductImage image, Product product);
}
