package by.epamlab.services.rest.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import by.epamlab.services.rest.beans.enums.Status;

@Entity
@Table(name = "shop_order")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_order")
	long id;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(name = "snapShoot_JSON", length = 5000)
	private String snapShootJSON;

	public Order() {
		super();
	}

	public long getId() {
		return id;
	}

	public String getSnapShootJSON() {
		return snapShootJSON;
	}

	public Status getStatus() {
		return status;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setSnapShootJSON(String snapShootJSON) {
		this.snapShootJSON = snapShootJSON;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", status=" + status + ", snapShootJSON=" + snapShootJSON + "]";
	}

}
