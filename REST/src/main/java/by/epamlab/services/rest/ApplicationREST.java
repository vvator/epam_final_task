package by.epamlab.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.ProductImage;
import by.epamlab.services.rest.beans.User;
import by.epamlab.services.rest.services.ProductImageServiceImpl;
import by.epamlab.services.rest.services.ProductServiceImpl;
import by.epamlab.services.rest.services.UserServiceImpl;

@ImportResource("classpath:/mvc-servlet-dispatcher.xml")
@RestController
@SpringBootApplication(scanBasePackages = "by")
public class ApplicationREST {

	public final Logger LOG = Logger.getLogger(ApplicationREST.class);

	@Autowired(required = true)
	@Qualifier("userService")
	private UserServiceImpl userService;

	public void setUserService(UserServiceImpl userService) {
		this.userService = userService;
	}

	@Autowired(required = true)
	@Qualifier("productService")
	private ProductServiceImpl productService;

	public void setProductService(ProductServiceImpl productService) {
		this.productService = productService;
	}

	@Autowired(required = true)
	@Qualifier("productImageService")
	private ProductImageServiceImpl productImageService;

	public void setProductImageService(ProductImageServiceImpl productImageService) {
		this.productImageService = productImageService;
	}

	////////////////////////////////////////////////////

	private static String impFolder = "images/products/";
	private static List<Product> list = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication appRest = new SpringApplication(ApplicationREST.class);
		appRest.run(args);

		List<ProductImage> imgUrls = new ArrayList<>();
		// imgUrls.add(impFolder+"shoes.jpg");
		imgUrls.add(new ProductImage(1,
				"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTKtXgz8uMHYerbOg2znqZSwDq_FBsTsBBofPOTmRPCdw6SRdliuw"));
		list.add(new Product(2, "Gentelman`s Shoes", "aasdfsssas", "22.50", "asasa", imgUrls));

		imgUrls = new ArrayList<>();
		// imgUrls.add(impFolder+"canon_exilim.jpg");
		imgUrls.add(new ProductImage(2,
				"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRotWJrQ2KveGx_-FtXlZjzOU4O9ehSwYmfxhtFpAKFvAWzEI18"));
		imgUrls.add(new ProductImage(3, "http://www.gems4mmines.com/Casio/Casio%20Exilim%20EX-TR-350%20Black-1.jpg"));
		imgUrls.add(new ProductImage(4, "http://www.gems4mmines.com/Casio/Casio%20Exilim%20EX-TR150%20Pink-2.jpg"));
		imgUrls.add(new ProductImage(5,
				"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTHRzmWmaLpoyy_63TUibSpPXzbYQ1ZljMgi7dNu2uRORMPjxdSmQ"));

		list.add(new Product(3, "Canon Exilim", "aassdfsas", "39.49", "asasa", imgUrls));
		imgUrls = new ArrayList<>();
		imgUrls.add(new ProductImage(6, impFolder + "armani_parfume.jpg"));
		list.add(new Product(4, "Armani Parfume", "aassas", "88.00", "asasa", imgUrls));
		imgUrls = new ArrayList<>();
		imgUrls.add(new ProductImage(7, impFolder + "wedding_shoes.jpg"));
		list.add(new Product(5, "Wedding Shoes", "aassas", "65.00", "asasa", imgUrls));
		imgUrls = new ArrayList<>();
		imgUrls.add(new ProductImage(8, impFolder + "canon_eos_5d.jpg"));
		list.add(new Product(6, "Canon EOS 5D", "aassas", "39.49", "asasa", imgUrls));
		imgUrls = new ArrayList<>();
		imgUrls.add(new ProductImage(9, impFolder + "htc_touch.jpg"));
		list.add(new Product(8, "HTC Touch", "aassas", "39.00", "asasa", imgUrls));
		imgUrls = new ArrayList<>();
		imgUrls.add(new ProductImage(10, impFolder + "nikon_d300.jpg"));
		list.add(new Product(9, "Nikon D300", "aassas", "39.49", "asasa", imgUrls));
		imgUrls = new ArrayList<>();
		imgUrls.add(new ProductImage(11, impFolder + "iphone.png"));
		list.add(new Product(1, "Iphone Classic", "aassas", "45.99", "asasa", imgUrls));

	}

	@RequestMapping("/")
	private String pi() {
		return "rest service is running";
	}

	/*
	 * @RequestMapping("/pp") public List<Product> greetingp(@RequestParam(value
	 * = "name", defaultValue = "World") String name, HttpServletResponse
	 * response) { response.setHeader("Content-Type", "application/json");
	 * response.setHeader("Access-Control-Allow-Origin", "*"); //
	 * "http://localhost:3000"
	 * response.setHeader("Access-Control-Allow-Methods",
	 * "GET,PUT,POST,DELETE");
	 * 
	 * return list; }
	 */
	@RequestMapping("/pw")
	@ResponseBody
	public void pw(@RequestBody List<Product> products, HttpServletResponse response) {
		response.setHeader("Content-Type", "application/json");
		response.setHeader("Access-Control-Allow-Origin", "*"); // "http://localhost:3000"
		response.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
		//////////// ????????????????????? B DAO

		productService.add(products);

		// return products;
		// return new ArrayList();
		// return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
		// return HttpStatus.OK;
	}

	/////////////////////////////////////////

	@RequestMapping(value = "/getUsersss", method = RequestMethod.POST)
	public @ResponseBody User getUser(@RequestBody User user, HttpServletResponse response) {
		/*
		 * response.setHeader("Content-Type", "application/json");
		 * response.setHeader("Access-Control-Allow-Origin", "*"); //
		 * "http://localhost:3000"
		 * response.setHeader("Access-Control-Allow-Methods",
		 * "GET,PUT,POST,DELETE");
		 */
		return user;
	}

	///////////////////////////////////////////////

}
