package by.epamlab.services.rest.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "shop_category_image")
public class CategoryImage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_category_image")
	private long id;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Category category;
	
	@Column(name = "url")
	private String url;

	public CategoryImage() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CategoryImage(long id, Category category, String url) {
		super();
		this.id = id;
		this.category = category;
		this.url = url;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CategoryImage [id=");
		builder.append(id);		
		builder.append(", url=");
		builder.append(url);
		builder.append("]");
		return builder.toString();
	}

}
