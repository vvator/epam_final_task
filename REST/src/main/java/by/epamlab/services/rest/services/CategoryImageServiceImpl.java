package by.epamlab.services.rest.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epamlab.services.rest.beans.Category;
import by.epamlab.services.rest.beans.CategoryImage;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.CategoryImageDaoImpl;
import by.epamlab.services.rest.services.interfaces.ICategoryImageService;

@Service
@Transactional
public class CategoryImageServiceImpl implements ICategoryImageService {
	private CategoryImageDaoImpl dao;

	public void setDao(CategoryImageDaoImpl dao) {
		this.dao = dao;
	}

	@Override
	public void add(CategoryImage image, Category category) {
		// TODO Auto-generated method stub
		dao.add(image,category);
	}
	
	@Override
	public void add(CategoryImage categoryImage) {
		dao.add(categoryImage);
	}

	@Override
	public CategoryImage getByID(long id) {
		return dao.getByID(id);
	}

	@Override
	public void update(CategoryImage categoryImage) {
		dao.update(categoryImage);
	}

	@Override
	public void delete(long id) {
		dao.delete(id);
	}

	@Override
	public List<CategoryImage> list() {
		return dao.list();
	}

	@Override
	public void add(List<CategoryImage> list) {
		dao.add(list);
	}

	@Override
	public List<CategoryImage> list(DataFilter<CategoryImage> filter) {
		return dao.list(filter);
	}

	

}
