package by.epamlab.services.rest.services.interfaces;

import by.epamlab.services.rest.beans.Category;
import by.epamlab.services.rest.beans.CategoryImage;

public interface ICategoryImageService extends IDAOService<CategoryImage> {
	  public void add(CategoryImage image, Category category);
}
