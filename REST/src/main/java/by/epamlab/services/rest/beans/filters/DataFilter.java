package by.epamlab.services.rest.beans.filters;

public abstract class DataFilter<T> {
	private long startPagination;
	private long countGet;
	
	
	public long getStartPagination() {
		return startPagination;
	}
	public void setStartPagination(long startPagination) {
		this.startPagination = startPagination;
	}
	public long getCountGet() {
		return countGet;
	}
	public void setCountGet(long countGet) {
		this.countGet = countGet;
	}
}
