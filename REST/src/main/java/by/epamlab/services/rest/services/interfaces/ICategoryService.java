package by.epamlab.services.rest.services.interfaces;

import by.epamlab.services.rest.beans.Category;

public interface ICategoryService extends IDAOService<Category> {

}
 