package by.epamlab.services.rest.dao.interfaces;

import by.epamlab.services.rest.beans.Category;
import by.epamlab.services.rest.beans.CategoryImage;

public interface ICategoryDao extends IDaoCRUID<CategoryImage> {
	public void add(CategoryImage image, Category category);
}
