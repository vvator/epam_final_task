package by.epamlab.services.rest.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.ProductDaoImpl;
import by.epamlab.services.rest.services.interfaces.IDAOService;

@Service
@Transactional
public class ProductServiceImpl implements IDAOService<Product> {
	private ProductDaoImpl dao;

	public void setDao(ProductDaoImpl dao) {
		this.dao = dao;
	}

	@Override
	public void add(Product product) {
		dao.add(product);
	}

	@Override
	public Product getByID(long id) {
		return dao.getByID(id);
	}

	@Override
	public void update(Product product) {
		dao.update(product);
	}

	@Override
	public void delete(long id) {
		dao.delete(id);
	}

	@Override
	public List<Product> list() {
		return dao.list();
	}

	@Override
	public void add(List<Product> list) {
		dao.add(list);

	}

	@Override
	public List<Product> list(DataFilter<Product> filter) {
		return dao.list(filter);
	}

}
