package by.epamlab.services.rest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.filters.FilterProdust;
import by.epamlab.services.rest.services.ProductImageServiceImpl;
import by.epamlab.services.rest.services.ProductServiceImpl;

@RestController
public class ProductController {

	@Autowired(required = true)
	@Qualifier("productService")
	private ProductServiceImpl productService;

	@Autowired(required = true)
	@Qualifier("productImageService")
	private ProductImageServiceImpl productImageService;

	@RequestMapping(value = "/createProduct", consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json")
	public ResponseEntity<Product> createProduct(@RequestBody Product product, HttpServletResponse response,
			HttpServletRequest request) {
		if (product != null) {
			productService.add(product);
			return new ResponseEntity<Product>(product, HttpStatus.OK);
		}
		return new ResponseEntity<Product>(product, HttpStatus.EXPECTATION_FAILED);
	}

	@RequestMapping(value = "/createProducts", consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json")
	public ResponseEntity<List<Product>> createProducts(@RequestBody List<Product> products,
			HttpServletResponse response, HttpServletRequest request) {
		List<Product> addProducts = new ArrayList<>();
		if (products != null) {
			addProducts.addAll(products);
			productService.add(addProducts);
			return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
		}
		return new ResponseEntity<List<Product>>(addProducts, HttpStatus.EXPECTATION_FAILED);
	}

	@RequestMapping("/products")
	public List<Product> getListProductsFromStore(HttpServletResponse response) {
		return productService.list();
	}

	@RequestMapping("/products/search={name}")
	public List<Product> getListProductsFromStoreByName(@PathVariable("name") String name,
			HttpServletResponse response) {
		FilterProdust filter = new FilterProdust();
		return productService.list(filter);
	}

	@RequestMapping(value = "/getProducta/{id}")
	public Product getProductssss(@PathVariable long id, HttpServletResponse response) {
		return productService.getByID(id);
	}

	@RequestMapping(value = "/product/{id}")
	@ResponseBody
	private ResponseEntity<Product> products(@PathVariable("id") long id, HttpServletResponse response) {
		Product product = productService.getByID(id);
		if (product != null) {
			return new ResponseEntity<Product>(product, HttpStatus.OK);
		}
		return new ResponseEntity<Product>(product, HttpStatus.NOT_FOUND);
	}

	public void setProductImageService(ProductImageServiceImpl productImageService) {
		this.productImageService = productImageService;
	}

	public void setProductService(ProductServiceImpl productService) {
		this.productService = productService;
	}

}
