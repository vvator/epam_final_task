package by.epamlab.services.rest.services.interfaces;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.ProductImage;

public interface IProductImageService extends IDAOService<ProductImage> {
	  public void add(ProductImage image, Product product);
}
