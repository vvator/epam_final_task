package by.epamlab.services.rest.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import by.epamlab.services.rest.beans.User;
import by.epamlab.services.rest.beans.UserPasswordChanger;
import by.epamlab.services.rest.beans.enums.Role;
import by.epamlab.services.rest.services.UserServiceImpl;

@Controller
@RestController
public class UserController {

	@Autowired(required = true)
	@Qualifier("userService")
	private UserServiceImpl userService;

	@RequestMapping("/getUser")
	public @ResponseBody User agreeting(@RequestBody User user, HttpServletResponse response) {
		return user;
	}

	@RequestMapping("/changePassword")
	public ResponseEntity<User> changePassword(@RequestBody UserPasswordChanger user) {
		if (user != null) {
			if (user.getPassword().equals(user.getPasswordConfirm())) {
				userService.changePassword(user);
			}
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@RequestMapping("/user-role-list")
	public @ResponseBody Role[] getUserRoleList(HttpServletResponse response) {
		Role[] listRole = Role.values();
		return listRole;
	}

	@RequestMapping("/users")
	public @ResponseBody List<User> getUsers(HttpServletResponse response) {
		List<User> users = userService.list();
		return users;
	}

	@RequestMapping("/createUser")
	public ResponseEntity<User> registryNewUser(@RequestBody User user) {
		userService.add(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	public void setUserService(UserServiceImpl userService) {
		this.userService = userService;
	}

	@RequestMapping("/signIn")
	public ResponseEntity<User> signIn(@RequestBody User user, HttpServletResponse response) {
		User userFind = new User();
		if (user != null) {
			user.setId(0);
			userFind = userService.getByEmail(user.getEmail());
			if (userFind != null) {
				return new ResponseEntity<User>(userFind, HttpStatus.OK);
			}
		}
		return new ResponseEntity<User>(userFind, HttpStatus.NOT_FOUND);
	}
}
