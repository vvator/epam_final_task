package by.epamlab.services.rest.dao.interfaces;

import java.util.List;

import by.epamlab.services.rest.beans.filters.DataFilter;

public interface IDaoCRUID<T> {
    public void add(T element);
    
    public void add(List<T> list);

    public T getByID(long id);

    public void update(T element);

    public void delete(long id);

    public List<T> list();

    public List<T> list(DataFilter<T> filter) ;
}
