package by.epamlab.services.rest.services.interfaces;

import java.util.List;

import by.epamlab.services.rest.beans.Order;
import by.epamlab.services.rest.beans.Product;

public interface IOrderService extends IDAOService<Order> {
	public void addOrderWithProducts(List<Product> products);

}
