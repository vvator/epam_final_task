package by.epamlab.services.rest.services.interfaces;

import by.epamlab.services.rest.beans.User;
import by.epamlab.services.rest.beans.UserPasswordChanger;

public interface IUserService extends IDAOService<User> {
	public User getByEmail(String email);

	public void changePassword(UserPasswordChanger user);

	public User signIn(User user);
}
