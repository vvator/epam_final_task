package by.epamlab.services.rest.config;

import org.springframework.context.annotation.ImportResource;

@ImportResource("classpath:mvc-servlet-dispatcher.xml")
public class SpringConfiguration {

}
