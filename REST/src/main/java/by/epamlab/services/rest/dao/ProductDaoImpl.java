package by.epamlab.services.rest.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.ProductImage;
import by.epamlab.services.rest.beans.ProductOption;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.interfaces.IDaoCRUID;

@Repository
public class ProductDaoImpl implements IDaoCRUID<Product> {

	private static final Logger log = LoggerFactory.getLogger(ProductDaoImpl.class);

	private SessionFactory sessionFactory;

	@Override
	public void add(List<Product> list) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			// action
			for (Product product : list) {
				product.setId(0);
				session.persist(product);
				for (ProductImage img : product.getImgUrls()) {
					img.setId(0);
					img.setProduct(product);
					session.persist(img);
				}

				for (ProductOption opt : product.getOptions()) {
					opt.setId(0);
					opt.setProduct(product);
					session.persist(opt);
				}
			}
			tx.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}

	@Override
	public void add(Product product) {
		Session session = sessionFactory.getCurrentSession();
		product.setId(0);
		session.persist(product);
		for (ProductImage img : product.getImgUrls()) {
			img.setId(0);
			img.setProduct(product);
			session.persist(img);
		}
		for (ProductOption opt : product.getOptions()) {
			opt.setId(0);
			session.persist(opt);
		}

		log.info("Product add: " + product);
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Product findProduct = getByID(id);
		if (findProduct != null) {
			session.delete(findProduct);
			log.info("Product deleted: " + findProduct);
		} else {
			log.info("Product NOT FOUND for delete... " + findProduct);
		}
	}

	@Override
	public Product getByID(long id) {
		Session session = sessionFactory.getCurrentSession();
		Product findProduct = session.get(Product.class, id);
		if (findProduct != null) {
			log.info("Product getByID(" + id + "): " + findProduct);
		} else {
			log.info("Product getByID(" + id + ")  NOT FOUND");
		}
		return findProduct;
	}

	@Override
	public List<Product> list() {
		Session session = sessionFactory.getCurrentSession();
		List<Product> list = (List<Product>) session.createQuery("from Product").list();
		if (!list.isEmpty()) {
			log.info("listProduct: \n\t" + list);
		} else {
			log.info("listProduct: \n\t" + list);
		}
		return list;
	}

	@Override
	public List<Product> list(DataFilter<Product> filter) {

		return null;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void update(Product product) {
		Session session = sessionFactory.getCurrentSession();
		Product findProduct = getByID(product.getId());
		if (findProduct != null) {
			session.update(product);
			log.info("Product update: \n\tOLD=> " + findProduct + "\n\tNEW=>" + product);
		} else {
			session.persist(product);
			log.info("Product NOT FOUND for update... Created new Product: " + product);
		}
	}

}
