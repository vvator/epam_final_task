package by.epamlab.services.rest.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epamlab.services.rest.beans.Order;
import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.interfaces.IDaoOrder;
import by.epamlab.services.rest.services.interfaces.IOrderService;

@Service
@Transactional
public class OrderServiceImpl implements IOrderService {

    private IDaoOrder dao;
    public void setDao(IDaoOrder dao) {
        this.dao = dao;
    }

    @Override
    public void add(Order element) {
        dao.add(element);
    }

    @Override
    public Order getByID(long id) {
        return  dao.getByID(id);
    }

    @Override
    public void update(Order element) {
        dao.update(element);
    }

    @Override
    public void delete(long id) {
        dao.delete(id);
    }

    @Override
    public List<Order> list() {
        return dao.list();
    }

	@Override
	public void add(List<Order> list) {
		dao.add(list);
	}

	@Override
	public List<Order> list(DataFilter<Order> filter) {
		return dao.list(filter);
	}

	@Override
	public void addOrderWithProducts(List<Product> products) {
		dao.addOrderWithProducts(products);
	}

	
}
