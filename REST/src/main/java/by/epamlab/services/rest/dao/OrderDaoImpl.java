package by.epamlab.services.rest.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import by.epamlab.services.rest.beans.Order;
import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.enums.Status;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.interfaces.IDaoOrder;

@Repository
public class OrderDaoImpl implements IDaoOrder {

	private static final Logger log = LoggerFactory.getLogger(OrderDaoImpl.class);

	private SessionFactory sessionFactory;

	@Override
	public void add(List<Order> list) {
		log.info("+++++++++++++++++++++++++++ +++");
		Session session = null;
		Transaction tx = null;
		// session = sessionFactory.getCurrentSession();
		// action
		for (Order order : list) {
			try {
				session = sessionFactory.openSession();
				tx = session.beginTransaction();
				order.setId(0);
				// ProductSnapShoot productSnapShoot = order.getProduct();
				// log.info("-->"+p);
				// ProductSnapShoot productSnapShoot = order.getProduct();
				// order.setProduct(productSnapShoot);
				// productSnapShoot.setOrder(order);
				session.persist(order);

				log.info("add +++" + order);
				tx.commit();
			} catch (Exception ex) {
				ex.printStackTrace();
				tx.rollback();
			} finally {
				session.close();
			}
		}
		// end action

	}

	public void add(Order element) {
		Session session = sessionFactory.getCurrentSession();
		element.setId(0);
		element.setStatus(Status.WAIT);
		session.persist(element);
		log.info("Order add: " + element);
	}

	@Override
	public void addOrderWithProducts(List<Product> products) {
		log.info("-*-*-*-*-*-*-*-*-*-*-");
		String json = "";
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		try {
			json = ow.writeValueAsString(products);
			log.info("\njson\n\n" + json);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		Session session = sessionFactory.getCurrentSession();

		Order order = new Order();
		log.info("Order add: " + order);
		order.setId(0);
		order.setStatus(Status.WAIT);
		order.setSnapShootJSON(json);

		session.persist(order);
		// end action
	}

	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Order findOrder = getByID(id);
		if (findOrder != null) {
			session.delete(findOrder);
			log.info("Order deleted: " + findOrder);
		} else {
			log.info("Order NOT FOUND for delete... " + findOrder);
		}
	}

	public Order getByID(long id) {
		Session session = sessionFactory.getCurrentSession();
		Order findOrder = session.load(Order.class, new Long(id));
		if (findOrder != null) {
			log.info("Order getByID(" + id + "): " + findOrder);
		} else {
			log.info("Order getByID(" + id + ")  NOT FOUND");
		}
		return findOrder;
	}

	public List<Order> list() {
		Session session = sessionFactory.getCurrentSession();
		List<Order> list = (List<Order>) session.createQuery("from Order").list();
		if (!list.isEmpty()) {
			log.info("listOrders: \n\t" + list);
		} else {
			log.info("listOrders: \n\t" + list);
		}
		return list;
	}

	@Override
	public List<Order> list(DataFilter<Order> filter) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void update(Order element) {
		Session session = sessionFactory.getCurrentSession();

		log.info("ddddddddddddddddddddddddddddddd");
		Order findOrder = getByID(element.getId());
		session.clear();
		if (findOrder != null) {
			log.info("sssssssssssssssss" + element);
			session.update(element);
			log.info("Order update: \n\tOLD=> " + findOrder + "\n\tNEW=>" + element);
		} else {
			// session.persist(element);
			log.info("Order NOT FOUND for update... Created new order: " + element);
		}

	}

}
