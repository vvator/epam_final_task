package by.epamlab.services.rest.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import by.epamlab.services.rest.beans.Category;
import by.epamlab.services.rest.beans.CategoryImage;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.interfaces.IDaoCRUID;

@Repository
public class CategoryDaoImpl implements IDaoCRUID<Category> {

	private static final Logger log = LoggerFactory.getLogger(CategoryDaoImpl.class);

	private SessionFactory sessionFactory;

	@Override
	public void add(Category category) {
		Session session = sessionFactory.getCurrentSession();
		category.setId(0);
		session.persist(category);
		log.info("\t add +++" + category);
		for (CategoryImage img : category.getImgUrls()) {
			img.setId(0);
			img.setCategory(category);
			session.persist(img);
			log.info("\t\t addImg +++" + img);
		}
		log.info("Category add: " + category);
	}

	@Override
	public void add(List<Category> list) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			// action
			for (Category category : list) {
				category.setId(0);
				session.persist(category);
				log.info(" add +++" + category);
				for (CategoryImage img : category.getImgUrls()) {
					img.setId(0);
					img.setCategory(category);
					session.persist(img);
					log.info(" addImg +++" + img);
				}
			}
			tx.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Category findCategory = getByID(id);
		if (findCategory != null) {
			session.delete(findCategory);
			log.info("Category deleted: " + findCategory);
		} else {
			log.info("Category NOT FOUND for delete... " + findCategory);
		}
	}

	@Override
	public Category getByID(long id) {
		Session session = sessionFactory.getCurrentSession();
		Category findCategory = session.get(Category.class, id);
		if (findCategory != null) {
			log.info("Category getByID(" + id + "): " + findCategory);
		} else {
			log.info("Category getByID(" + id + ")  NOT FOUND");
		}
		return findCategory;
	}

	@Override
	public List<Category> list() {
		Session session = sessionFactory.getCurrentSession();
		List<Category> list = (List<Category>) session.createQuery("from Category").list();
		if (!list.isEmpty()) {
			log.info("listCategory: \n\t" + list);
		} else {
			log.info("listCategory: \n\t" + list);
		}
		return list;
	}

	@Override
	public List<Category> list(DataFilter<Category> filter) {
		
		return null;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void update(Category category) {
		Session session = sessionFactory.getCurrentSession();
		Category findCategory = getByID(category.getId());
		if (findCategory != null) {
			session.update(category);
			log.info("Category update: \n\tOLD=> " + findCategory + "\n\tNEW=>" + category);
		} else {
			session.persist(category);
			log.info("Category NOT FOUND for update... Created new Category: " + category);
		}
	}

}
