package by.epamlab.services.rest.dao.interfaces;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.ProductImage;

public interface IProductDao extends IDaoCRUID<Product> {
	public void add(ProductImage image, Product product);
}
