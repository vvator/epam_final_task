package by.epamlab.services.rest.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "shop_product_image")
public class ProductImage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_product_image")
	private long id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Product product;

	@Column(name = "url")
	private String url;

	public ProductImage() {
		super();

	}

	public ProductImage(long id, String url) {
		super();
		this.id = id;
		this.url = url;
	}

	public long getId() {
		return id;
	}

	public Product getProduct() {
		return product;
	}

	public String getUrl() {
		return url;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductImage [id=");
		builder.append(id);
		builder.append(", url=");
		builder.append(url);
		builder.append("]");
		return builder.toString();
	}

}
