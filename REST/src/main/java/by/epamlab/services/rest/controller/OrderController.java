package by.epamlab.services.rest.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import by.epamlab.services.rest.beans.Order;
import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.enums.Status;
import by.epamlab.services.rest.services.OrderServiceImpl;

@Controller
@RestController
public class OrderController {

	private final Logger LOG = Logger.getLogger(OrderController.class);

	@Autowired(required = true)
	@Qualifier("orderService")
	private OrderServiceImpl orderService;

	@RequestMapping("/getOrder/{id}")
	public @ResponseBody Order agreeting(@PathVariable("id") int id, HttpServletResponse response) {
		return orderService.getByID(id);
	}

	@RequestMapping("/orders")
	public @ResponseBody List<Order> getOrders(HttpServletResponse response) {
		List<Order> orders = orderService.list();
		return orders;

	}

	@RequestMapping("/order-status-list")
	public @ResponseBody Status[] getOrdersStatusList(HttpServletResponse response) {
		Status[] listStatus = Status.values();
		return listStatus;
	}

	@RequestMapping("/buy")
	public ResponseEntity<Order> registryNewOrder(@RequestBody Object products) {
		String json = "";
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		try {
			json = ow.writeValueAsString(products);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		Order order = new Order();
		if (order != null) {
			orderService.addOrderWithProducts((List<Product>) products);
		}
		return new ResponseEntity<Order>(order, HttpStatus.OK);
	}

	public void setOrderService(OrderServiceImpl orderService) {
		this.orderService = orderService;
	}

	@RequestMapping("/updateOrder")
	public ResponseEntity<Order> updateOrder(@RequestBody Order order) {
		if (order != null) {
			orderService.update((Order) order);
		}
		return new ResponseEntity<Order>((Order) order, HttpStatus.OK);
	}

}
