package by.epamlab.services.rest.dao.interfaces;

import by.epamlab.services.rest.beans.User;
import by.epamlab.services.rest.beans.UserPasswordChanger;

public interface IDaoUser extends IDaoCRUID<User> {
	public User getByEmail(String email);

	public void changePassword(UserPasswordChanger user);

	public User signIn(User user);

}
