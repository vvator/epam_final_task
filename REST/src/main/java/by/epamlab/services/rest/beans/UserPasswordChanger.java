package by.epamlab.services.rest.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserPasswordChanger extends User {
	@JsonIgnore
	private String oldPassword;

	public UserPasswordChanger() {
		super();

	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserPasswordChanger [oldPassword=");
		builder.append(oldPassword);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
