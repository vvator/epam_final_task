package by.epamlab.services.rest.beans.enums;

public enum Role {
	UNKNOWN, USER, ADMIN
}
