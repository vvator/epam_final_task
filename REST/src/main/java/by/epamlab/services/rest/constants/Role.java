package by.epamlab.services.rest.constants;

public enum Role {
	USER, ADMIN, SUPER_ADMIN
}
