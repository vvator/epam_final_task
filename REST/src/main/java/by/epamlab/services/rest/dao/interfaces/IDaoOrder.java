package by.epamlab.services.rest.dao.interfaces;

import java.util.List;

import by.epamlab.services.rest.beans.Order;
import by.epamlab.services.rest.beans.Product;

public interface IDaoOrder extends IDaoCRUID<Order>  {
	public void addOrderWithProducts(List<Product> products);
}
