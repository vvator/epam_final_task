package by.epamlab.services.rest.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import by.epamlab.services.rest.beans.User;
import by.epamlab.services.rest.beans.UserPasswordChanger;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.interfaces.IDaoUser;

@Repository
public class UserDaoImpl implements IDaoUser {

	private static final Logger log = LoggerFactory.getLogger(UserDaoImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void add(User element) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(element);
		log.info("User add: " + element);
	}

	public User getByID(long id) {
		Session session = sessionFactory.getCurrentSession();
		User findUser = session.load(User.class, new Long(id));
		if (findUser != null) {
			log.info("User getByID(" + id + "): " + findUser);
		} else {
			log.info("User getByID(" + id + ")  NOT FOUND");
		}
		return findUser;
	}

	public void update(User element) {
		Session session = sessionFactory.getCurrentSession();

		User findUser = getByID(element.getId());
		if (findUser != null) {
			session.update(element);
			log.info("User update: \n\tOLD=> " + findUser + "\n\tNEW=>" + element);
		} else {

			session.persist(element);
			log.info("User NOT FOUND for update... Created new user: " + element);
		}
		session.close();
	}

	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		User findUser = getByID(id);
		if (findUser != null) {
			session.delete(findUser);
			log.info("User deleted: " + findUser);
		} else {
			log.info("User NOT FOUND for delete... " + findUser);
		}
	}

	public List<User> list() {
		Session session = sessionFactory.getCurrentSession();
		List<User> list = (List<User>) session.createQuery("from User").list();
		if (!list.isEmpty()) {
			log.info("listUsers: \n\t" + list);
		} else {
			log.info("listUsers: \n\t" + list);
		}
		return list;
	}

	@Override
	public void add(List<User> list) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			// action
			for (User user : list) {
				session.persist(user);
				log.info("\t add +++" + user);
			}
			// end action

			tx.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}

	@Override
	public List<User> list(DataFilter<User> filter) {
		return null;
	}

	@Override
	public User getByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		User user = (User) session.createQuery("from User u where u.email = :email").setString("email", email).getSingleResult();
		
		if (user != null) {
			log.info(" User getByEmail(" + email + "): \n\t" + user);
			return user;
		} else {
			log.info(" NOT FOUND User getByEmail(" + email + "): \n\t" + user);
		}
		return null;

	}
	
	@Override
	public User signIn(User user) {
		Session session = sessionFactory.getCurrentSession();
		User findUser = getByEmail(user.getEmail());
		
		if (findUser != null && findUser.equals(user.getPassword()) ) {
			log.info("USER SIGN IN: "+findUser);
			return findUser;
		} else {
			log.info(" AUTHENTIFICATION FAILED User:"+ user);
		}
		return null;

	}

	@Override
	public void changePassword(UserPasswordChanger user) {
		Session session = sessionFactory.getCurrentSession();
		User findUser = getByID(user.getId());

		if (findUser != null) {
			if (findUser.getPassword().equals(user.getPassword())) {
				update(user);
			}
			log.info(" changePassword: \n\t" + user);
		} else {
			log.info(" NOT changePassword: \n\t" + user);
		}

	}
}
