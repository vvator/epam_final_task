package by.epamlab.services.rest.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import by.epamlab.services.rest.beans.Category;
import by.epamlab.services.rest.beans.CategoryImage;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.interfaces.ICategoryDao;

@Repository
public class CategoryImageDaoImpl implements ICategoryDao {

	private static final Logger log = LoggerFactory.getLogger(CategoryImageDaoImpl.class);

	private SessionFactory sessionFactory;

	@Override
	public void add(CategoryImage categoryImage) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(categoryImage);
	}

	@Override
	public void add(CategoryImage image, Category category) {
		Session session = sessionFactory.getCurrentSession();
		image.setCategory(category);
		session.persist(image);
	}

	@Override
	public void add(List<CategoryImage> list) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		CategoryImage findCategoryImage = getByID(id);
		if (findCategoryImage != null) {
			session.delete(findCategoryImage);
			log.info("CategoryImage deleted: " + findCategoryImage);
		} else {
			log.info("CategoryImage NOT FOUND for delete... " + findCategoryImage);
		}
	}

	@Override
	public CategoryImage getByID(long id) {
		Session session = sessionFactory.getCurrentSession();
		CategoryImage findCategoryImage = session.load(CategoryImage.class, id);
		if (findCategoryImage != null) {
			log.info("CategoryImage getByID(" + id + "): " + findCategoryImage);
		} else {
			log.info("CategoryImage getByID(" + id + ")  NOT FOUND");
		}
		return findCategoryImage;

	}

	@Override
	public List<CategoryImage> list() {
		Session session = sessionFactory.getCurrentSession();
		List<CategoryImage> list = (List<CategoryImage>) session.createQuery("from CategoryImage").list();
		if (!list.isEmpty()) {
			log.info("listCategoryImage: \n\t" + list);
		} else {
			log.info("listCategoryImage: \n\t" + list);
		}
		return list;
	}

	@Override
	public List<CategoryImage> list(DataFilter<CategoryImage> filter) {
		return null;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void update(CategoryImage categoryImage) {
		Session session = sessionFactory.getCurrentSession();
		CategoryImage findCategoryImage = getByID(categoryImage.getId());
		if (findCategoryImage != null) {
			session.update(categoryImage);
			log.info("CategoryImage update: \n\tOLD=> " + findCategoryImage + "\n\tNEW=>" + categoryImage);
		} else {
			session.persist(categoryImage);
			log.info("CategoryImage NOT FOUND for update... Created new CategoryImage: " + categoryImage);
		}
	}

}
