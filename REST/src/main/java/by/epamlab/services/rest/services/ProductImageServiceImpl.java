package by.epamlab.services.rest.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epamlab.services.rest.beans.Product;
import by.epamlab.services.rest.beans.ProductImage;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.ProductImageDaoImpl;
import by.epamlab.services.rest.services.interfaces.IProductImageService;

@Service
@Transactional
public class ProductImageServiceImpl implements IProductImageService {
	private ProductImageDaoImpl dao;

	public void setDao(ProductImageDaoImpl dao) {
		this.dao = dao;
	}

	@Override
	public void add(ProductImage image, Product product) {
		// TODO Auto-generated method stub
		dao.add(image,product);
	}
	
	@Override
	public void add(ProductImage productImage) {
		dao.add(productImage);
	}

	@Override
	public ProductImage getByID(long id) {
		return dao.getByID(id);
	}

	@Override
	public void update(ProductImage productImage) {
		dao.update(productImage);
	}

	@Override
	public void delete(long id) {
		dao.delete(id);
	}

	@Override
	public List<ProductImage> list() {
		return dao.list();
	}

	@Override
	public void add(List<ProductImage> list) {
		dao.add(list);
	}

	@Override
	public List<ProductImage> list(DataFilter<ProductImage> filter) {
		return dao.list(filter);
	}

	

}
