package by.epamlab.services.rest.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "shop_category")
public class Category {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_category")
	private long id;
	@Column(name = "name")
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "imgUrls")
	@ManyToMany(mappedBy = "category", fetch = FetchType.LAZY)
	private List<CategoryImage> imgUrls = new ArrayList<CategoryImage>();

	public Category() {
		super();

	}

	public Category(long id, String name, String description, List<CategoryImage> imgUrls) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.imgUrls = imgUrls;
	}

	public String getDescription() {
		return description;
	}

	public long getId() {
		return id;
	}

	public List<CategoryImage> getImgUrls() {
		return imgUrls;
	}

	public String getName() {
		return name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setImgUrls(List<CategoryImage> imgUrls) {
		this.imgUrls = imgUrls;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Category [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", imgUrls=");
		builder.append(imgUrls);
		builder.append("]");
		return builder.toString();
	}

}
