package by.epamlab.services.rest.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "shop_product_option")
public class ProductOption {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_product_image")
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "value")
	private String value;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Product product;

	/*
	 * @JsonIgnore
	 * 
	 * @ManyToMany(fetch = FetchType.LAZY)
	 * 
	 * @JoinTable(name = "shop_po", joinColumns = { @JoinColumn(name =
	 * "option_ID") }, inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "product_ID") }) private List<Product> product = new
	 * ArrayList<Product>();
	 */

	public ProductOption() {
		super();
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Product getProduct() {
		return product;
	}

	public String getValue() {
		return value;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductOption [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", value=");
		builder.append(value);
		builder.append("]");
		return builder.toString();
	}

}
