package by.epamlab.services.rest.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epamlab.services.rest.beans.User;
import by.epamlab.services.rest.beans.UserPasswordChanger;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.interfaces.IDaoUser;
import by.epamlab.services.rest.services.interfaces.IUserService;

@Service
@Transactional
public class UserServiceImpl implements IUserService {

	private IDaoUser dao;

	public void setDao(IDaoUser dao) {
		this.dao = dao;
	}

	@Override
	public void add(User element) {
		dao.add(element);
	}

	@Override
	public User getByID(long id) {
		return dao.getByID(id);
	}

	@Override
	public void update(User element) {
		dao.update(element);
	}

	@Override
	public void delete(long id) {
		dao.delete(id);
	}

	@Override
	public List<User> list() {
		return dao.list();
	}

	@Override
	public void add(List<User> list) {
		dao.add(list);
	}

	@Override
	public List<User> list(DataFilter<User> filter) {
		return dao.list(filter);
	}

	@Override
	public User getByEmail(String email) {
		return dao.getByEmail(email);
	}

	public void changePassword(UserPasswordChanger user) {
		dao.changePassword(user);

	}

	@Override
	public User signIn(User user) {
		return dao.signIn(user);
	}

}
