package by.epamlab.services.rest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import by.epamlab.services.rest.beans.Category;
import by.epamlab.services.rest.services.CategoryImageServiceImpl;
import by.epamlab.services.rest.services.CategoryServiceImpl;

@RestController
public class CategoryController {

	@Autowired(required = true)
	@Qualifier("categoryService")
	private CategoryServiceImpl categoryService;

	@Autowired(required = true)
	@Qualifier("categoryImageService")
	private CategoryImageServiceImpl categoryImageService;

	@RequestMapping(value = "/category/{id}")
	@ResponseBody
	private ResponseEntity<Category> categories(@PathVariable("id") long id, HttpServletResponse response) {
		Category category = categoryService.getByID(id);
		if (category != null) {
			return new ResponseEntity<Category>(category, HttpStatus.OK);
		}
		return new ResponseEntity<Category>(category, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/createCategories", consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json")
	public ResponseEntity<List<Category>> createCategories(@RequestBody List<Category> categories,
			HttpServletResponse response, HttpServletRequest request) {
		List<Category> addCategories = new ArrayList<>();
		if (categories != null) {
			addCategories.addAll(categories);
			categoryService.add(addCategories);
			return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);
		}
		return new ResponseEntity<List<Category>>(addCategories, HttpStatus.EXPECTATION_FAILED);
	}

	@RequestMapping(value = "/createCategory", consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json")
	public ResponseEntity<Category> createCategory(@RequestBody Category category, HttpServletResponse response,
			HttpServletRequest request) {
		if (category != null) {
			categoryService.add(category);
			return new ResponseEntity<Category>(category, HttpStatus.OK);
		}
		return new ResponseEntity<Category>(category, HttpStatus.EXPECTATION_FAILED);
	}

	@RequestMapping(value = "/getCategory/{id}")
	public Category getCategoriessss(@PathVariable long id, HttpServletResponse response) {
		return categoryService.getByID(id);
	}

	@RequestMapping("/categories")
	public List<Category> getListCategoriesFromStore(HttpServletResponse response) {
		return categoryService.list();
	}

	public void setCategoryImageService(CategoryImageServiceImpl categoryImageService) {
		this.categoryImageService = categoryImageService;
	}

	public void setCategoryService(CategoryServiceImpl categoryService) {
		this.categoryService = categoryService;
	}

}
