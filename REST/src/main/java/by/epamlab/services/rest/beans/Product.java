package by.epamlab.services.rest.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "shop_product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_product")
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "category")
	private String category;
	@Column(name = "price")
	private String price;

	@Column(name = "stocked")
	private String stocked;

	@Column(name = "options")
	@ManyToMany(mappedBy = "product", fetch = FetchType.LAZY)
	private List<ProductOption> options = new ArrayList<ProductOption>();
	/*
	 * @ManyToMany(cascade = {CascadeType.ALL})
	 * 
	 * @JoinTable(name="shop_po", joinColumns={@JoinColumn(name="product_ID")},
	 * inverseJoinColumns={@JoinColumn(name="option_ID")}) private
	 * Set<ProductOption> options = new HashSet<ProductOption>();
	 */
	@Column(name = "imgUrls")
	@ManyToMany(mappedBy = "product", fetch = FetchType.LAZY)
	private List<ProductImage> imgUrls = new ArrayList<ProductImage>();

	public Product() {
		super();

	}

	public Product(long id, String name, String category, String price, String stocked, List<ProductImage> imgUrls) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.price = price;
		this.stocked = stocked;
		this.imgUrls = imgUrls;
	}

	public String getCategory() {
		return category;
	}

	public long getId() {
		return id;
	}

	public List<ProductImage> getImgUrls() {
		return imgUrls;
	}

	public String getName() {
		return name;
	}

	public List<ProductOption> getOptions() {
		return options;
	}

	public String getPrice() {
		return price;
	}

	public String getStocked() {
		return stocked;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setImgUrls(List<ProductImage> imgUrl) {
		this.imgUrls = imgUrl;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOptions(List<ProductOption> options) {
		this.options = options;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setStocked(String stocked) {
		this.stocked = stocked;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", price=");
		builder.append(price);
		builder.append(", stocked=");
		builder.append(stocked);
		builder.append("]");
		return builder.toString();
	}

}
