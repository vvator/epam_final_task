package by.epamlab.services.rest.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epamlab.services.rest.beans.Category;
import by.epamlab.services.rest.beans.filters.DataFilter;
import by.epamlab.services.rest.dao.CategoryDaoImpl;
import by.epamlab.services.rest.services.interfaces.IDAOService;

@Service
@Transactional
public class CategoryServiceImpl implements IDAOService<Category> {
	private CategoryDaoImpl dao;

	public void setDao(CategoryDaoImpl dao) {
		this.dao = dao;
	}



	@Override
	public void add(Category category) {
		dao.add(category);
	}
	
	

	@Override
	public Category getByID(long id) {		
		return dao.getByID(id);
	}

	@Override
	public void update(Category category) {
		dao.update(category);
	}

	@Override
	public void delete(long id) {
		dao.delete(id);
	}

	@Override
	public List<Category> list() {		
		return dao.list();
	}



	@Override
	public void add(List<Category> list) {
		dao.add(list);
		
	}



	@Override
	public List<Category> list(DataFilter<Category> filter) {		
		return dao.list(filter);
	}

}
