package by.epamlab.services.rest.beans.enums;

public enum Status{
	WAIT("Wait"),
	UNCATEGORIZED("Uncategorized"),
	ACCEPTED("Accepted"),
	SEND("Send"),
	DELIVERED_TO_CUSTOMER("Delivered to customer"),
	CANCELED_BY_CUSTOMER("Canceled by customer");
	
	
	private String name;

	private Status(String name) {
		this.name = name;
	}	
	
}	