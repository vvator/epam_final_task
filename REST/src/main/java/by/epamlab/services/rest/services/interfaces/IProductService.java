package by.epamlab.services.rest.services.interfaces;

import by.epamlab.services.rest.beans.Product;

public interface IProductService  extends IDAOService<Product> {

}
