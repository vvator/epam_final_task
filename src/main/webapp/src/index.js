import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';

import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {Router,Route,IndexRoute,/*hashHistory,*/browserHistory} from 'react-router';

import {syncHistoryWithStore} from 'react-router-redux';
import combineReducers from './js/reducers/index';

import App from './App';
import HomeView from './js/components/views/home/home';
import SigninView from './js/components/views/user/signin/signin';
import ProductView from './js/components/views/product/product';
import CartView from './js/components/views/cart/cart';
import ProductsView from './js/components/views/product/products';
import CategoryView from './js/components/views/category/category';
import AccountView from './js/components/views/user/account/account';
import RegistrationView from './js/components/views/user/registration/registration';
import IncorrectLocationView from './js/components/views/location-incorrect';
import CategoriesView from './js/components/views/category/categories';

import OrdersView from './js/components/views/cart/orders/orders';


const store = createStore(combineReducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
const history = syncHistoryWithStore(browserHistory,store);



ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
        <Route path='/' component={App}>
            <IndexRoute component={HomeView}/>
            <Route path='home' component={HomeView}/>
            <Route path='admin' component={SigninView}/>
            <Route path='orders' component={OrdersView}/>
            <Route path='cart' component={CartView}/>
            <Route path='products' component={ProductsView}/>
            <Route path='product/:id' component={ProductView}/>
            <Route path='categories' component={CategoriesView}/>
            <Route path='category/:id' component={CategoryView}/>
            <Route path='cart' component={CartView}/>
            <Route path='account' component={AccountView}/>
            <Route path='registration' component={RegistrationView}/>
            <Route path='signin' component={SigninView}/>

            <Route path="*" component={IncorrectLocationView}/>
        </Route >
    </Router>
  </Provider>
  ,
  document.getElementById('root')
);
