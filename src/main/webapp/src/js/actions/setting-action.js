const log = console
    .log
    .bind(this);

const settingsActionDispatcher = dispatch => ({

    onSetView: (view) => {
        log('SETTINGS_VIEW:', view);
        dispatch({type: 'SETTINGS_VIEW', view});
    }
    
})

export default settingsActionDispatcher;