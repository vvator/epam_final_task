const log = console
    .log
    .bind(this);

const orderActionDispatcher = dispatch => ({
     onLoadOrders: () => {
        fetch("http://localhost:8081/orders")
            .then(orders => orders.json())
            .then(orders => {
                console.log(orders);
                dispatch({type: 'ORDER_LOAD', orders});
            });
    },
    onMinusToCart: (product) => {
        log('ORDER_MINUS:', product);
        dispatch({type: 'ORDER_MINUS', product});
    }, 
    onAddToCart: (product) => {
        log('ORDER_ADD:', product);
        dispatch({type: 'ORDER_ADD', product});
    },
    onChangeCountToCart: (product, count) => {
        log('ORDER_CHANGE_COUNT:' + count);        
        log(product);
        let orderRecord={
                product,
                count
            }
        dispatch({
            type: 'ORDER_CHANGE_COUNT',
            orderRecord
        });
    },
    onCartDelete: (product) => {
        log('ORDER_DELETE:', product);
        dispatch({type: 'ORDER_DELETE', product})
    },
    onCartClear: () => {
        log('ORDER_CLEAR');
        dispatch({type: 'ORDER_CLEAR'})
    },

    ////////////////////////////////////////////
    onBUY: (order) => {
       /* order.id = 0;
        order.status = 'DELIVERED_TO_CUSTOMER';
        
        log('ORDER_BUY');
        log('---------->' + JSON.stringify(order));
        fetch("http://localhost:8081/buy")
            .then(order => order.json())
            .then(order => {
                log('<-----' + order);
                dispatch({type: 'ORDER_BUY', order})
            });
*/

        order.id = 0;
        order.status = 'DELIVERED_TO_CUSTOMER';
        log('ORDER_BUY');
        log('---------->' + JSON.stringify(order));
        fetch("http://localhost:8081/buy", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },

                body: JSON.stringify(order)
            })
            .then(order => order.json())
            .then(order => {
                log('<-----' + order);
                dispatch({type: 'ORDER_BUY', order})
            });


    },





    setOrderStatus: (order)=>{
        log('ORDER_UPDATE');
        log('================================> '+order.status)
        log('---------->' + JSON.stringify(order));
        fetch("http://localhost:8081/updateOrder", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },

                body: JSON.stringify(order)
            })
            .then(order => order.json())
            .then(order => {
                log('<-----' + order);
                dispatch({type: 'ORDER_UPDATE', order})
            });

    }
})

export default orderActionDispatcher;