const log = console
    .log
    .bind(this);

const categoryActionDispatcher = dispatch => ({

    onAddCategories: (categoriesToAdd) => {
        console.log(JSON.stringify(categoriesToAdd));
        fetch("http://localhost:8081/createCategories", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
                body: JSON.stringify(categoriesToAdd)
            })
            .then(categories => categories.json())
            .then(categories => {
                log(categories);
                dispatch({type: 'CATEGORY_ADD', categories});
            });
    },

    onLoadCategories: () => {
        fetch("http://localhost:8081/categories")
            .then(categories => categories.json())
            .then(categories => {
                console.log(categories);
                dispatch({type: 'CATEGORIES_LOAD', categories});
            });
    },
    onLoadCategory: (id) => {
        fetch("http://localhost:8081/category/" + id)
            .then(category => category.json())
            .then(category => {
                console.log(category);
                dispatch({type: 'CATEGORY_LOAD', category});
            });
    }
})

export default categoryActionDispatcher;