const log = console
    .log
    .bind(this);

const cartActionDispatcher = dispatch => ({
    onMinusToCart: (product) => {
        log('CART_MINUS:', product);
        dispatch({type: 'CART_MINUS', product});
    }, 
    onAddToCart: (product) => {
        log('CART_ADD:', product);
        dispatch({type: 'CART_ADD', product});
    },
    onChangeCountToCart: (product, count) => {
        log('CART_CHANGE_COUNT:' + count);        
        log(product);
        let cartRecord={
                product,
                count
            }
        dispatch({
            type: 'CART_CHANGE_COUNT',
            cartRecord
        });
    },
    onCartDelete: (product) => {
        log('CART_DELETE:', product);
        dispatch({type: 'CART_DELETE', product})
    },
    onCartClear: () => {
        log('CART_CLEAR');
        dispatch({type: 'CART_CLEAR'})
    },

    ////////////////////////////////////////////
    onBUY: (order) => {
        order.id = 0;
        order.status = 'WAIT';
        
        log('CART_BUY');
        log('---------->' + JSON.stringify(order));
        fetch("http://localhost:8081/buy", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },

                body: JSON.stringify(order)
            })
            .then(order => order.json())
            .then(order => {
                log('<-----' + JSON.stringify(order));

                dispatch({type: 'CART_BUY', order})
            });

    } 
})

export default cartActionDispatcher;