const orderStatusActionDispatcher = dispatch => ({
  onLoadListStatus: () => {
    fetch("http://localhost:8081/order-status-list")
      .then(orderStatus => orderStatus.json())
      .then(orderStatus => {
        console.log(orderStatus);
        dispatch({type: 'ORDER_STATUS_LOAD', orderStatus});
      });
  }
})

export default orderStatusActionDispatcher;