const log = console
    .log
    .bind(this);

const userActionDispatcher = dispatch => ({
    onRegistration: (registrationInfo) => {
        fetch("http://localhost:8081/createUser", {
            //redentials: 'include', //pass cookies, for authentication
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                /*, 'X-Random-Header':'123123123'*/
            },
                /* mode: 'no-cors',
                //headers:{
                //  'Access-Control-Allow-Origin': 'http://localhost:3000',
                //  'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
                //  'Content-Type': 'application/json'

                //  'Access-Control-Allow-Origin': '*',
                //  'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
                //  'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
                //  'Content-Type': 'application/json; charset=utf-8',
                //  'X-Random-Header':'123123123'*/
                // }, headers: {  'Accept': 'application/json',  'Accept': 'application/json,
                // application/xml, text/plain, text/html, *.*',  'Content-Type':
                // 'application/json; charset=utf-8' }, mode: 'cors',*/
                body: JSON.stringify(registrationInfo)
            })
            .then(user => user.json())
            .then(user => {
                dispatch({type: 'USER_ADD', user});
                console.log(JSON.stringify(user));
                console.log(user);
            });
    },

    onSignIn: (registrationInfo) => {
        registrationInfo.role = 'UNKNOWN';

        function status(response) {
            console.log(response.status);
            if (response.status >= 200 && response.status < 300) {
                //return
                return Promise.resolve(response)
            } else {
                //alert('lol');
                return Promise.reject(new Error(response.statusText))
            }
        }

        console.log(JSON.stringify(registrationInfo));
        fetch("http://localhost:8081/signIn", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
                body: JSON.stringify(registrationInfo)
            })
            .then(status)
            .then(info => info.json())
            .then(info => {
                console.log(JSON.stringify(info));
                console.log(info);
                log('USER_SIGNIN:', info);
                dispatch({type: 'USER_SIGNIN', info});

            })
            .catch(function (error) {
                console.log('AUTH FAILED ', error);

            });;

    },
    onLogout: () => {
        console.log('USER_LOGOUT');
        dispatch({type: 'USER_LOGOUT'});
    },
    onChangePassword: (user) => {
        console.log('+++++++++++++', user)
        fetch("http://localhost:8081/changePassword", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
                body: JSON.stringify(user)
            })
            .then(info => info.json())
            .then((info) => {
                console.log(info);
                console.log('USER_CHANGE_PASSWORD:', info);
                dispatch({type: 'USER_CHANGE_PASSWORD', info});
            });
    },

    onAddToCart: (user) => {
        log('USER_ADD:', user);
        dispatch({type: 'USER_ADD', user});
    },
    onChangeCountToCart: (user, count) => {
        log('USER_CHANGE_COUNT:', count, user);
        dispatch({type: 'USER_CHANGE_COUNT', user, count});
    },
    onMinusToCart: (user) => {
        log('USER_MINUS:', user);
        dispatch({type: 'USER_MINUS', user});
    },
    onCartDelete: (user) => {
        log('USER_DELETE:', user);
        dispatch({type: 'USER_DELETE', user})
    },
    onCartClear: () => {
        log('USER_DELETE');
        dispatch({type: 'USER_CLEAR'})
    },

    ////////////////////////////////////////////
    onBUY: (order) => {
        log('USER_BUY');
        log(JSON.stringify(order));
        fetch("http://localhost:8081/buy", {
            /*redentials: 'include', //pass cookies, for authentication*/
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                /*  'X-Random-Header':'123123123'*/
            },
                /*mode: 'no-cors',
                //headers:{
                //  'Access-Control-Allow-Origin': 'http://localhost:3000',
                //  'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
                //  'Content-Type': 'application/json'

                //  'Access-Control-Allow-Origin': '*',
                //  'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
                //  'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
                //  'Content-Type': 'application/json; charset=utf-8',
                //  'X-Random-Header':'123123123'*/
                // }, headers: {  'Accept': 'application/json',  'Accept': 'application/json,
                // application/xml, text/plain, text/html, *.*',  'Content-Type':
                // 'application/json; charset=utf-8'  }, mode: 'cors',*/
                body: JSON.stringify(order)
            })
            .then(order => order.json())
            .then(order => {
                log(order);
                dispatch({type: 'USER_BUY', order})
            });

    },
    onLoadUserRole: () => {
        fetch("http://localhost:8081/user-role-list")
            .then(userRoleList => userRoleList.json())
            .then(userRoleList => {
                console.log(userRoleList);
                dispatch({type: 'USER_ROLE_LOAD', userRoleList});
            });
    }
})

export default userActionDispatcher;