const initialState = [
{id:'',name:'',category:'',price:'',stocked:'',imgUrls:''},
];


export default function filterProducts(
    state = initialState, action
) {
    switch (action.type) {
        case 'PRODUCT_FILTER':
            return action.product;
        
        default:
            return state;
    }
}