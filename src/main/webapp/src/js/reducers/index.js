import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux';

import products from './products';
import categories from './categories';
import cart from './cart';
import user from './user';
import userRoleList from './userRoleList';
import orderStatus from './orderStatus';
import filterProducts from './filterProducts';

import settings from './settings';
import order from './order';



//import categoryReducer from './categoryReducer';

export default combineReducers({
    routing: routerReducer,   
    order,
    products,
    categories,
    settings,
    orderStatus,
    user,
    userRoleList,
    filterProducts,
    
    cart
    //,
    //categoryReducer

}); 