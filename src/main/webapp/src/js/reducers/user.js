const initialState = {
    id: 1,
    firstName: '1',
    lastName: '1',
    adress: '1',
    phone: '1',
    email: '1',
    password: '1',
    role: 'ADMIN'
  };

export default function user(state = initialState, action) {

    switch (action.type) {
        case 'USER_SIGNIN':
            console.log('OK', action.info);
            // let cookie = ["user", '=', JSON.stringify(state), '; domain=.',
            // window.location.host.toString(), '; path=/;'].join(''); let cookie =
            // "user="+JSON.stringify(action.info); document.cookie = cookie;
            return action.info;

        case 'USER_LOGOUT':
              console.log('OK+++');
            return {}
        case 'USER_CHANGE_PASSWORD':
            console.log('OK');
            //Location.reload();
            return {};
            
        case 'USER_ADD':
            return action.user;

        case 'USER_CLEAR':
            return {};

        case 'USER_DELETE':
            return [...state.filter(user => user.id !== action.user.id)];

        case 'USER_BUY':
            console.log("-------------------------------------");
            return [
                ...state.orders,
                Object.assign({}, action.order)
            ];
            
        default:
            return state;
    }

}
