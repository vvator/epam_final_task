const initialState = {
   viewElement: 'DETAILS'
};


export default function config(state = initialState, action) {
    switch (action.type) {
       case 'CONFIG_SET_ELEMENTS_VIEW':
            let newState = state;
            console.log('-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-',action.view);
            newState.viewElement = action.view;
            return newState; 
      
        default:
            return state;   
    }
}