const initialState = [];

export default function cart(state = initialState, action) {
  
    let findProductInCart;

    switch (action.type) { 
        case 'CART_ADD': 
           // let cart = [];
            console.log('=========>');
            console.log(action.product);            
            findProductInCart = state.filter(cartElement => cartElement.product.id === action.product.id);
            if (findProductInCart.length === 0) {
                let order = {
                   // id: 0,
                   // status: 0,
                    countInCart: 1,
                    product: action.product
                }
                return [
                    ...state.filter(cartElement => cartElement.productid !== action.product.id),
                    Object.assign({}, order)
                ];
            } else {
                findProductInCart[0].countInCart++;
                return [...state];
            }

        case 'CART_MINUS':
            findProductInCart = state.filter(cartElement => (cartElement.product.id === action.product.id) && (cartElement.countInCart > 0));
            if (findProductInCart.length > 0) {
                findProductInCart[0].countInCart--;
            }
            return [...state];

        case 'CART_CHANGE_COUNT': 
            let cardRecord = action.cartRecord;
            console.log('-0-0-0-0-0-0-0-0-0-'+cardRecord.count);          
            console.log(cardRecord.product); 
            let product = cardRecord.product.product;  
             
            console.log('-==========-'+cardRecord.count);  
            console.log('-==========-'+product.name); 
            let find =[]; 
            find = [...state.filter(cartElement => {
            console.log(cartElement.product.id+"---"+product.id); 
                 console.log(cartElement.product.id === product.id);
                 return cartElement.product.id === product.id;
            /* && (cartElement.count > 0)*/
            })];
            console.log(find.length);
            if (find.length > 0) {
                console.log('-1-1-1-1-1-1-1-1-1-'+cardRecord.count); 
                find[0].countInCart = cardRecord.count;
            } 
            return [...state];

        case 'CART_CLEAR':
            return [];

        case 'CART_DELETE':
        console.log('action: '+action.product.product.id)
            return [...state.filter(cartElement => cartElement.product.id !== action.product.product.id)];

        case 'CART_BUY':
            console.log("-------------------------------------");
            return [...state];
        default:
            return state;
    }
}
