const initialState = {
    view:'DETAILS'
   /*
    "id":"18",
    "firstName":"6",
    "lastName":"6",
    "adress":"6",
    "phone":"6",
    "email":"6",
    "role":"ADMIN"
*/
};

export default function settings(state = initialState, action) {

    switch (action.type) {
        case 'SETTINGS_VIEW':
            console.log('ok: '+action.view)
            return ({view:action.view});
            
        default:
            return state;
    }

}
