import React, {Component} from 'react'
import {connect} from 'react-redux';
import { Link } from 'react-router';

class SearchComponent extends Component {
    onClickBtnSearch(){
        console.log("aaaaaaaaaaaaaaaaaa",this.inputSearch.value);
    }

    render() {
        return (       
                <div className="top-menu-right">
                    <div className="top-menu-frm">
                        <form action="#" method="get" acceptCharset="utf-8">
                            <input type="text" className="search" placeholder="Search this site..."  ref={(input) => {this.inputSearch = input}}/>
                            <input type="submit" onClick={this.onClickBtnSearch.bind(this)} className="search-btn" value=""/>                               
                                 <Link to="/products">aaaa</Link>
                        </form>
                    </div>
                    <ul>
                        <li>
                            <a href=""><img src="images/social/facebook.png" role="presentation"/>
                            </a>
                            
                        </li>
                        <li>
                            <a href="">
                                <img src="images/social/twitter.png" role="presentation"/>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="images/social/linkedin.png" role="presentation"/>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="images/social/rss.png" role="presentation"/>
                            </a>
                        </li>
                    </ul>
                </div>
        )
    }
}
export default connect(
    state => ({
        products: state.products
    }), 
    dispatch => ({
    onAddProduct: () => {
        console.log('===================');
        fetch("http://localhost:8081/products")
            .then(response => response.json())
            .then(json => {
                console.log(json);
                dispatch({type: 'PRODUCT_LOAD', json});
            });

    }
}),
/*dispatch =>({
     onAddProduct: (products) => {
      dispatch({type:'PRODUCT_LOAD', products});
   }}),*/
//  cartActionDispatcher//???????????????????????
)(SearchComponent);
//export default connect(state => ({cart: state.cart}), dispatch => ({}))(SearchComponent);
