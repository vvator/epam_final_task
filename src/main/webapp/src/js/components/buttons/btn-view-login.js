import React, { Component } from 'react';
import {Link } from 'react-router';


class ButtonLoginView extends Component {
    render() {
        return (
            <div>
                <Link to="/signIn">SIGN IN</Link>
            </div>
        );
    }
}

export default ButtonLoginView;