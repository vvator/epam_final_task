import React, { Component } from 'react';

class ArrowsPreviewNextView extends Component {
    render() {
        return (
            <div className="navigation">
                <a href="#" className="preview-list" />
                <a href="#" className="next-list" />
            </div>
        );
    }
}

export default ArrowsPreviewNextView;