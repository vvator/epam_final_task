import React, {Component} from 'react'



class View extends Component {
    render () {
        return (
            <div className="content-wrapper">              
                    {this.props.child}                    
            </div>
        )
    }
}

export default View;