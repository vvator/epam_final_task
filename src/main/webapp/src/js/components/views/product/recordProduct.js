import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router";

import cartActionDispatcher from '../../../actions/cart-actions';
import ViewComponent from '../../views/cart/view';

import noImage from '../../../../image/noImage.png';
class RecordProduct extends Component{  

generateRows(products){
    let rowStyle={
        display:'block',        
        background:'gray',
        clear:'both'

    }

let styleDetails = {
    border: "1px solid #ddd",
    borderRadius: "2px",
    float: "left",
    margin: "0 5px 5px 0",
    padding: "0",
    width: "100%",
    display: "block",
    clear: "both"
}



    let rows = [];
    for(let i=0; i<products.length;i++){
    let product= products[i];
    
        let pr = (this.props.settings.view==='GRID')?
             <div className="product-pnl" key={product.id}>
                <div className="product-header-pnl">                
                    <Link to={"/product/"+product.id}>{product.name}</Link>                   
                </div>
                <div className="priduct-body-pnl">
                    <div className="product-img-pnl">                      
                      
                        <img src={(product.imgUrls.length !== 0)? product.imgUrls[0].url : noImage } role="presentation"/>
                   
                        <div className="product-price">{product.price}</div>
                    </div>
                    <div className="product-footer-pnl">
                        <Link onClick={this.props.onAddToCart.bind(this,product)} className="product-btn-add">ADD TO CART</Link>
                        <Link to={"/product/"+product.id} className="product-btn-more">MORE INFO</Link>
                    </div>
                </div>
            </div>
           :
          <div className="product-pnl" key={product.id} style={styleDetails}>
                <div className="product-header-pnl">                
                    <Link to={"/product/"+product.id}>{product.name}</Link>                   
                </div>
                <div className="priduct-body-pnl">
                    <div className="product-img-pnl">                      
                      
                        <img src={(product.imgUrls.length !== 0)? product.imgUrls[0].url : noImage } role="presentation"/>
                   
                        <div className="product-price">{product.price}</div>
                    </div>
                    <div className="product-footer-pnl">
                        <Link onClick={this.props.onAddToCart.bind(this,product)} className="product-btn-add">ADD TO CART</Link>
                        <Link to={"/product/"+product.id} className="product-btn-more">MORE INFO</Link>
                    </div>
                </div>
            </div>;

           



        
            
        rows.push(pr);
    }
    return rows;
    }

    render(){
        let rows = this.generateRows(this.props.products);
        return(           
            <div>    
                {rows}      
            </div>
        );
    }
}




export default connect(
  state =>({
    products: state.products,
    settings: state.settings,

    filterProducts: state.filterProducts
  }),
    cartActionDispatcher
)(RecordProduct);
