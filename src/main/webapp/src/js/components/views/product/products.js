import React, { Component } from 'react';

import ProductAdder from './product-adder';
import ProductFilter from './product-filter';
//import {connect} from 'react-redux';
import ProductListSquare from './product-list-square/product-list-square';
import ItemTypeView from '../../menus/item-type-view/item-type-view';
import ArrowsPreviewNextView from '../../buttons/arrows-previos-next';   
import RightSidebar from '../../views/sidebar-right/sidebar-right';


class ProductsView extends Component {
  render() {
     
    return (

<div className="content-wrapper">
                <ItemTypeView/>
                <ArrowsPreviewNextView/>    
                <div className="left-sidebar">
                    <div className="content">





                      
                      
                              <h3>PRODUCT VIEW</h3>                                                          
                              <ProductAdder />
                              <hr/>
                              <ProductFilter/>
                              <hr/>
                              <ProductListSquare/>
                        
                      
                     









                    </div>
                    
                    <div className="pagination">
                      <div id="link-next-product">
                          <a href="#">Next Product »</a>
                      </div>
                      <div id="link-home"><a id="index.html">Home</a></div>
                    </div>   

                </div>
                <RightSidebar/>
            </div>

    );
  }
}

export default ProductsView;