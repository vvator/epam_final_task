import React, {Component} from 'react';
import {connect} from 'react-redux';
import categoryActionDispatcher from '../../../actions/category-actions';
class SelectCategory extends Component {

    state = ({})
    componentWillMount() {
      //  if(JSON.stringify(this.props.categories) === JSON.stringify([])){
        this
            .props
            .onLoadCategories();       
    }

    render() {
        let options = [];
        let name = "id";
        this.props
            .categories
            .forEach((category,index) => {
                options.push(
                    <option key={index} value={JSON.stringify(category)}>{category[name]}</option>                
                )
            })
        return (
            <div>
                <select value={this.state.value} onChange={this.props.onValue}>
                    {options}
                </select>
            </div>
        );
    }       
}

export default connect(
    state => ({
        categories: state.categories
    }), 
    categoryActionDispatcher
    )(SelectCategory);