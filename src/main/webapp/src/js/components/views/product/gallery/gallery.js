import React, {Component} from 'react'
import noImage from '../../../../../image/noImage.png';
/*import {connect} from 'react-redux';*/
//import { Link } from "react-router";

class ProductImageGalery extends Component {

    state = {
        product: this.props.product
    };

    onClickToMinImg(url) {
        console.log('url:', url);
        this.setState(state => ({srcUrlMain: url}));
    }

    render() {
        let [product] = [this.props.product];

        let imgArr = [];
        Array
            .prototype
            .push
            .apply(imgArr, this.props.product.imgUrls);

        let imgMain = noImage;
        if (!this.state.srcUrlMain) {
            imgMain = (imgArr.length !== 0)
                ? imgArr[0].url
                : noImage;
        } else {
            imgMain = this.state.srcUrlMain;
        }

        let arrMiniPicture = [];
        Array
            .prototype
            .push
            .apply(arrMiniPicture, product.imgUrls);
        let mainIMG = <img height="200px" src={imgMain} role="presentation"/>;

        const divStyle = {
            display: 'inline-block',
            padding: '5px',
            margin: '3px',
            border: '1px solid black'
        };
        let rows = [];
        arrMiniPicture.map((im, index) => {
            rows.push(
                <div style={divStyle} key={index}>
                    <a
                        onClick={this
                        .onClickToMinImg
                        .bind(this, im.url)}>
                        <img src={im.url} height="50px;" role="presentation"/>
                    </a>
                    <div>{index + 1}</div>
                </div>
            )
            return im;
        })

        const styleGaleryMain = {
            display: 'inline-block',
            height: '300px',
            border: '1px solid black'
        }

        return (
            <div style={styleGaleryMain}>
                {mainIMG}
                <hr/> 
                {rows}
                <hr/>
            </div>
        )
    }
}

export default ProductImageGalery;