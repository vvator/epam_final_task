import React, {Component} from 'react';
import {connect} from 'react-redux';
import userActionDispatcher from '../../../actions/user-action';
class SelectUserRole extends Component {

    state = ({})
    componentWillMount() {
      //  if(JSON.stringify(this.props.userRoleList) === JSON.stringify([])){
        this
            .props
            .onLoadUserRole();       
    }

    render() {
        let options = [];
        this.props
            .userRoleList
            .forEach((role,index) => {
                options.push(
                    <option key={index} value={JSON.stringify(role)}>{role}</option>                
                )
            })
        return (
            <div>
                <select value={this.state.value} onChange={this.props.onValue}>
                    {options}
                </select>
            </div>
        );
    }       
}

export default connect(
    state => ({
        userRoleList: state.userRoleList
    }), 
    userActionDispatcher
    )(SelectUserRole);