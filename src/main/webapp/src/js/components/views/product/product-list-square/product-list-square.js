import React, {Component} from 'react';
import {connect} from 'react-redux';
import RecordProduct from '../recordProduct';
import productActionDispatcher from '../../../../actions/product-actions';

class ProductListSquare extends Component {

    componentWillMount() {      
        this.props.onGetProducts();
    }

    render() {
        return (
            <div className="content">             
                <RecordProduct/>
            </div>
        );
    }
}

export default connect(state => ({
    products: state.products
    }),     
    productActionDispatcher
)(ProductListSquare);