import React, {Component} from 'react';
import {connect} from 'react-redux';
//import productActionDispatcher from '../../../actions/product-actions';
import cartActionDispatcher from '../../../actions/cart-actions';
import ProductImageGalery from './gallery/gallery';
import { Link } from 'react-router';
import srcAddToCart from '../../../../image/add_to_cart-green.png';


class ProductView extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }
  
  state = {
    product: {}
  };

  componentWillMount() {

   // this.props.onProductGet(this.props.params.id)
   let idP = this.props.params.id;
   let product =
    fetch("http://localhost:8081/product/" + idP)
      .then(response => response.json())
      .then(product => {
        console.log(product);
        this.setState({product: product});
        // dispatch({type: 'PRODUCT_LOAD', json});
      });
    this.setState({product: product});
  }

  addToCart(product){
    this.props.onAddToCart(product);
  }
  

showOptionProduct(){

}

genUniqID(){
    return Date.now().toString();
}
  render() {
 
   //let options = this.showOptionProduct(this.state.product);
        let options = [];
       Array
            .prototype
            .push
            .apply(options,  this.state.product.options);

        
            let optionP = [];
            for (let opt of options) {
                optionP.push(
                    <div key={this.genUniqID() +opt.id}>                       
                            {opt.name} = {opt.value};    
                    </div>
                );             
            }           
       return (
        <div className="content-wrapper">
          <div className="ProductView">
            <h3>PRODUCT </h3>
            {optionP} 
            <hr/>
              {this.state.product.id}
              {this.state.product.name}
              {this.state.product.category}
              {this.state.product.price}
              {this.state.product.stocked}
            <hr/>

            <img src={srcAddToCart} role="presentation"/>
            <Link onClick={this.addToCart.bind(this, this.state.product)}    className="product-btn-add">ADD TO CART</Link>
            <hr/>   
              <ProductImageGalery {...this.state}/>
          
            <hr/>
          </div>
        </div>
    );
  }
}



export default connect(
  state => ({
    products: state.products
  }), 
  cartActionDispatcher//,
//  productActionDispatcher
  )(ProductView);
