import React, {Component} from 'react';

import {connect} from 'react-redux';
import SelectCategory from './select-list-category';
import SelectOrderStatus from './select-list-orderStatus';

import productActionDispatcher from '../../../actions/product-actions';

class ProductAdder extends Component {
    state = {
        cbCategory: {},
        product: {
            imgUrls: []
        },
        products: []
    };

    clearAllFromLocal() {
        this.setState({products: []});
    };

    onDeleteProductFromLocalClick(product) {
        let products = this
            .state
            .products
            .filter(itemProduct => {
                return itemProduct.id !== product.id;
            });
        this.setState({products: products});
    }

    addStateProductClick() {
        let add_product = this.fillProductFromInputData();
        let findProductInCart = this
            .state
            .products
            .filter(product => product.name === this.fillProductFromInputData().name);
        if (findProductInCart.length === 0) {
            this.setState({
                products: [
                    ...this
                        .state
                        .products
                        .filter(product => product.name !== this.fillProductFromInputData().name),
                    Object.assign({}, add_product)
                ]
            })
        } else {
            add_product = findProductInCart[0];
            this.setState({
                products: [...this.state.products]
            })
        }
    }

    addOptionToProductClick(_options_) {
        let add_option = this.fillProductFromInputData();
        console.log('++++++++++++++++++++++++++++++++++++++++++++', _options_);
        console.log('++++++++++++++++++++++++++++++++++++++++++++', add_option.options);
        Array
            .prototype
            .push
            .apply(_options_, add_option.options);
        console.log('++++++++++++++++++++++++++++++++++++++++++++', _options_);
        this.setState({
            ...this.state.products
        })
    }

    addImgToProductClick(_product_) {
        let add_product = this.fillProductFromInputData();
        console.log('++++++++++++++++++++++++++++++++++++++++++++', _product_);
        console.log('++++++++++++++++++++++++++++++++++++++++++++', add_product.imgUrls);
        Array
            .prototype
            .push
            .apply(_product_, add_product.imgUrls);
        console.log('++++++++++++++++++++++++++++++++++++++++++++', _product_);
        this.setState({
            ...this.state.products
        })

    }

    fillProductOption() {
        let inputOptionName = this.inputOptionName.value;
        let inputOptionValue = this.inputOptionValue.value;

        let option = {
            id: 0,
            name: inputOptionName,
            value: inputOptionValue
        }

        console.log('fillProductOption:', option);
        return option;
    }

    fillProductFromInputData() {

        console.log(this.state.cbCategory);
        let lol = this.state.cbCategory;
        console.log(lol.id);
        let inputName = this.inputName.value;
        let inputCategory = this.inputCategory.value;
        let inputPrice = this.inputPrice.value;
        let inputStocked = this.inputStocked.value;
        let inputImgUrls = this.inputImgUrl.value;
        let inputOptionName = this.inputOptionName.value;
        let inputOptionValue = this.inputOptionValue.value;

        if (inputName === '') {
            return;
        }

        let product = {
            id: this.genUniqID(),
            name: inputName,
            category: inputCategory,
            price: inputPrice,
            stocked: inputStocked,
            options: [
                {
                    id: this.genUniqID(),
                    name: inputOptionName,
                    value: inputOptionValue
                }
            ],
            imgUrls: [
                {
                    id: this.genUniqID(),
                    url: inputImgUrls
                }
            ]
        };
        console.log('fillProductFromInputData:', product);
        return product;
    }

    addStateProductsClick() {
        console.log('addStateProductsClick:', this.state.products);
        this
            .props
            .onAddProducts(this.state.products);
        this.inputName.value = '';
        console.log(JSON.stringify(this.state.products));
    };

    addClick() {
        console.log('addClick:', this.fillProductFromInputData());
        this
            .props
            .onAddProduct(this.fillProductFromInputData());
        this.inputName.value = '';
    };

    addFilterClick() {
        let inputId = this.inputFilterId.value;
        let inputName = this.inputFilterName.value;
        let inputCategory = this.inputFilterCategory.value;
        let inputPrice = this.inputFilterPrice.value;
        let inputStocked = this.inputFilterStocked.value;
        let inputImgUrl = this.inputFilterImgUrl.value;

        console.log('onFilterProduct:', inputId, inputName, inputCategory, inputPrice, inputStocked, inputImgUrl);
        this
            .props
            .onFilterProduct(inputId, inputName, inputCategory, inputPrice, inputStocked, inputImgUrl);
    }

    genUniqID() {
        return Date
            .now()
            .toString();
    }

    onDeleteImgFromProductLocal(img) {
        console.log("delete image from product ", img);
    }

    render() {

        let styleMinPic = {
            display: 'inline-block',
            height: '50px',
            background: '#CDCDCD',
            margin: '3px'
        };

        const styleNewAdd = {
            background: 'gray'
        };

        let notSend = [];
        for (let product of this.state.products) {

            let imagesProduct = product.imgUrls;
            let imagesP = [];
            for (let img of imagesProduct) {
                imagesP.push(
                    <div key={this.genUniqID() + img.id} style={styleMinPic}>
                        <a>
                            <img src={img.url} height="50px;" role="presentation"/>
                        </a>
                        <div>
                            <button
                                onClick={this
                                .onDeleteImgFromProductLocal
                                .bind(this, img)}>
                                x
                            </button>
                        </div>
                    </div>
                );
            };

            let optionProducts = product.options;
            let optionP = [];
            for (let opt of optionProducts) {
                optionP.push(
                    <div key={this.genUniqID() + opt.id}>
                        {opt.name}
                        = {opt.value};
                        <div>
                            x
                        </div>
                    </div>
                );
            };

            notSend.push(
                <div key={this.genUniqID() + product.name}>
                    <div>
                        {product.name}| {product.category}| {imagesP}

                        <input
                            type="text"
                            placeholder="url image"
                            ref={
                                (input) => {
                                    this.inputImgUrl = input
                                }
                            }
                            default="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQa6wYcqvV70VdmsR-GZdl51GX3ZfNESy1pQXyLt2xtpi-1CiwDVA"/>
                        <button
                            onClick={this
                            .addImgToProductClick
                            .bind(this, product.imgUrls)}>+img+</button>
                        <button
                            onClick={this
                            .onDeleteProductFromLocalClick
                            .bind(this, product)}>x</button>
                        <div>
                            {optionP}
                        </div>
                        name
                        <input
                            type="text"
                            placeholder="param name"
                            ref={(input) => {
                            this.inputOptionName = input
                        }}/>
                        value
                        <input
                            type="text"
                            placeholder="param value"
                            ref={(input) => {
                            this.inputOptionValue = input
                        }}/>
                        <button
                            onClick={this
                            .addOptionToProductClick
                            .bind(this, product.options)}>+option+</button>
                        <button
                            onClick={this
                            .onDeleteProductFromLocalClick
                            .bind(this, product)}>x</button>
                    </div>
                </div>
            )
        }

        return (
            <div>

                <br/>
                <hr/> {/*id: */}
                <input
                    type="hidden"
                    ref={(input) => {
                    this.inputId = input
                }}/>
                name:
                <input
                    type="text"
                    ref={(input) => {
                    this.inputName = input
                }}/>
                category:
                <input
                    type="text"
                    ref={(input) => {
                    this.inputCategory = input
                }}/>
                price:
                <input
                    type="text"
                    ref={(input) => {
                    this.inputPrice = input
                }}/>
                stocked:
                <input
                    type="text"
                    ref={(input) => {
                    this.inputStocked = input
                }}/>
                imgUrl:
                <input
                    type="text"
                    ref={(input) => {
                    this.inputImgUrl = input
                }}
                    default="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQa6wYcqvV70VdmsR-GZdl51GX3ZfNESy1pQXyLt2xtpi-1CiwDVA"/>
                option n
                <input
                    type="text"
                    ref={(input) => {
                    this.inputOptionName = input
                }}/>
                option v
                <input
                    type="text"
                    ref={(input) => {
                    this.inputOptionValue = input
                }}/>

                <SelectCategory
                    value="qqqqqq"
                    onValue={this
                    .onValueCategorySelect
                    .bind(this)}/>
                <SelectOrderStatus
                    value="qqqqqq"
                    onValue={this
                    .onValueOrderStatusSelect
                    .bind(this)}/>

                <button
                    onClick={this
                    .fillProductFromInputData
                    .bind(this)}>***</button>

                <button
                    onClick={this
                    .addStateProductClick
                    .bind(this)}>+saveOnPage</button>

                <hr/>
                <button
                    onClick={this
                    .addClick
                    .bind(this)}>+</button>
                <hr/>
                <div style={styleNewAdd}>
                    {notSend}
                    <button
                        onClick={this
                        .clearAllFromLocal
                        .bind(this)}>+clear All +</button>
                    <button
                        onClick={this
                        .addStateProductsClick
                        .bind(this)}>+save All from local To BD+</button>
                </div>
                <hr/>

            </div>
        );
    }

    onValueCategorySelect(e) {
        // this.refs.selectCategory.value;
        let a = JSON.parse(e.target.value);
        console.log(a)
        this.setState({cbCategory: a});
    }
    onValueOrderStatusSelect(e) {
        // this.refs.selectCategory.value;
        let a = JSON.parse(e.target.value);
        console.log(a)
        this.setState({cbOrderStatus: a});
    }
}

export default connect(state => ({settings: state.settings, filterProducts: state.filterProducts}), productActionDispatcher)(ProductAdder);
