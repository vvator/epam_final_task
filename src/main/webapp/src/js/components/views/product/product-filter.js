import React, { Component } from 'react';

import {connect} from 'react-redux';


import productActionDispatcher from  '../../../actions/product-actions';
//import RecordProduct from './js/components/views/product/recordProduct';



class ProductFilter extends Component {
  state = {    
    product: { imgUrls:[]},
    products: []
  };

addFilterClick(){
    let inputId = this.inputFilterId.value;
    let inputName = this.inputFilterName.value;
    let inputCategory = this.inputFilterCategory.value;
    let inputPrice = this.inputFilterPrice.value;
    let inputStocked = this.inputFilterStocked.value;
    let inputImgUrl = this.inputFilterImgUrl.value;
   
   /* if(inputName === ''){
       return;
}*/

    console.log('onFilterProduct:',inputId,inputName,inputCategory,inputPrice,inputStocked,inputImgUrl);
    this.props.onFilterProduct(inputId,inputName,inputCategory,inputPrice,inputStocked,inputImgUrl);
}


  render() {
  
  let notSend = [];
  for (let product of  this.state.products) {
    let imagesProduct = product.imgUrls;

    let imagesP = [];
    for (let img of imagesProduct) {      
        imagesP.push(<a key={img.url}>  <img src={img.url} height="50px;" role="presentation"/>  </a>);       
    }
    notSend.push(
      <div key={product.id}>
        {product.name}| {product.category}|
        {imagesP}
        <input type="text" ref={(input)=> {this.inputImgUrl = input}}  default="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQa6wYcqvV70VdmsR-GZdl51GX3ZfNESy1pQXyLt2xtpi-1CiwDVA"/>        
        <button onClick={this.addImgToProductClick.bind(this,product.imgUrls)}>+img+</button>
        <button onClick={this.onDeleteProductFromLocalClick.bind(this,product)}>x</button>
      </div>      
    )


  }
  



    return (
      
      <div>


       <h1>filter:</h1>
       <hr/>

        id: 
        {/*<input type="hidden"  onChange={this.addFilterClick.bind(this)} ref={(input)=> {this.inputFilterId = input}} />*/}
        <input type="text"  onChange={this.addFilterClick.bind(this)} ref={(input)=> {this.inputFilterId = input}}/>
        name:
        <input type="text"  onChange={this.addFilterClick.bind(this)} ref={(input)=> {this.inputFilterName = input}} />
        category:
        <input type="text"  onChange={this.addFilterClick.bind(this)} ref={(input)=> {this.inputFilterCategory = input}} />
        price:
        <input type="text"  onChange={this.addFilterClick.bind(this)} ref={(input)=> {this.inputFilterPrice = input}} />
        stocked:
        <input type="text"  onChange={this.addFilterClick.bind(this)} ref={(input)=> {this.inputFilterStocked = input}} />
        imgUrl:
        <input type="text"  onChange={this.addFilterClick.bind(this)} ref={(input)=> {this.inputFilterImgUrl = input}} />
        
        <button onClick={this.addFilterClick.bind(this)}>filter</button>
      
       <hr/>
        ** {this.props.products.length}**
        
        {/*<RecordProduct products={this.props.products}/>*/}
        
        ***

      </div>
    );
  }
}






export default connect(
  state =>({
    products: state.products /*
    .filter(product => product.name.includes(state.filterProducts.name))
    .filter(product => product.category.includes(state.filterProducts.category))
    .filter(product => product.price.includes(state.filterProducts.price))
    .filter(product => product.stocked.includes(state.filterProducts.stocked))    
    */
    ,    
    filterProducts: state.filterProducts
  }),
   productActionDispatcher
)(ProductFilter);

