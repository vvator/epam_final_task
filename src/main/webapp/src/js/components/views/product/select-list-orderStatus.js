import React, {Component} from 'react';
import {connect} from 'react-redux';
import orderStatusActionDispatcher from '../../../actions/orderStatus-actions';

class SelectOrderStatus extends Component {
    state = ({})
    componentWillMount() {
      //  if(JSON.stringify(this.props.orderStatus) === JSON.stringify([])){
        this
            .props
            .onLoadListStatus();       
    }

    render() {
        let orderStatusList = [];
        this.props
            .orderStatus
            .forEach((status,index) => {
                orderStatusList.push(
                    <option key={index} value={status}>{status}</option>                
                )
            })
        return (
            <div>
                <select value={this.state.value} onChange={this.props.onValue}>
                    {orderStatusList}
                </select>
            </div>
        );
    }       
}

export default connect(
    state => ({
        orderStatus: state.orderStatus
    }), 
    orderStatusActionDispatcher
    )(SelectOrderStatus);