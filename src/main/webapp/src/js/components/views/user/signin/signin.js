import React, {Component} from 'react';

import {Link} from 'react-router';
import {connect} from 'react-redux';
import userActionDispatcher from '../../../../actions/user-action';
import signIn_logo from '../../../../../image/tables/sign-in.png';
import '../../../../../styles/default/custom.css'
import '../../../../../styles/default/product-pnl.css'

class SigninView extends Component {

  fillProductFromInputData() {
    let firstName = '';
    let lastName = '';
    let adress = '';
    let phone = '';
    let email = this.inputSignInEmail.value;
    let password = this.inputSignInPasssword.value;
    let passwordConfirm = this.inputSignInPasssword.value;;
    let role = 'UNKNOWN';

    let registrationInfo = {
      firstName,
      lastName,
      adress,
      phone,
      email,
      password,
      passwordConfirm,
      role
    };
    return registrationInfo;
  }

  onSignIn() {
    let registrationInfo = this.fillProductFromInputData();
    this
      .props
      .onSignIn(registrationInfo)
  }

  render() {

    let view = <div className="SigninView">
                  <h3>SIGNIN</h3>
                  <img src={signIn_logo} role="presentation"/>

                  <form action="#" method="get" acceptCharset="utf-8">
                    <label htmlFor="login">Email:</label>
                    <input type="text" name="email" placeholder="email" className="form-component" //required="required"
                      //pattern=".{5,}"
                      ref={(input) => {
                      this.inputSignInEmail = input
                    }} title="email"/>
                    <label htmlFor="password">Password:</label>
                    <input type="password" name="password" placeholder="password" className="form-component" // required="required"
                      ref={(input) => {
                      this.inputSignInPasssword = input
                    }} title="<password></password>"/>
                    <label htmlFor="save">
                      <input type="checkbox" name="save" className="form-component" defaultValue="save" //required="required"
                        title="save"/>
                      save
                    </label>
                    <Link
                      onClick={this
                      .onSignIn
                      .bind(this)}
                      className="form-component">Enter</Link>
                    <Link to="/registration" className="form-component">Registration</Link>
                  </form>
               </div>;
   

    return (
      <div>
        {view}
      </div>
    );
  }
}

export default connect(state => ({user: state.user}), userActionDispatcher)(SigninView);
