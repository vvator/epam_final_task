import React, { Component } from 'react';
import {connect} from 'react-redux';
import ButtonLoginView from '../../../buttons/btn-view-login';
import userActionDispatcher from '../../../../actions/user-action';
import SelectUserRole from '../../product/select-list-user-role';
class RegistrationView extends Component {
 
     fillProductFromInputData() {
        let firstName = this.inputRegistrationFirstName.value;
        let lastName = this.inputRegistrationLastName.value;
        let adress = this.inputRegistrationAdress.value;
        let phone = this.inputRegistrationPhone.value;
        let email = this.inputRegistrationEmail.value;
        let password = this.inputRegistrationPassword.value;
        let passwordConfirm = this.inputRegistrationPasswordConfirm.value;
        let role =  'UNKNOWN';
        if (firstName === '') {
            return;
        }

        let registrationInfo = {
            firstName,
            lastName,
            adress,
            phone,
            email,
            password,
            passwordConfirm,
            role
        };
        console.log(JSON.stringify(registrationInfo));
       return registrationInfo;
    }

    onSent(){
      let registrationInfo = this.fillProductFromInputData();     
      this.props.onRegistration(registrationInfo);

    }

 onValue(e){
    // this.refs.selectCategory.value;
    let a =  JSON.parse(e.target.value);
    console.log(a)
    this.setState({role:  a});
 }

  render() {
    return (
      <div className="RegistrationView">
      REGISTRATION VIEW


 
<ButtonLoginView/>

      <table className="registration-form">
            
            <tbody>
              <tr>
                <td>
                  <label htmlFor="first-name">
                    First Name*:
                  </label>
                </td>
                <td>
                  <input
                    type="text"
                    className="form-component"
                    name="last-name"
                    pattern="[A-Za-zА-Яа-яЁё]{3,20}"
                    title="[A-Za-zА-Яа-яЁё]{3,20}"
                    required="true"
                    ref={(input) => {this.inputRegistrationFirstName = input}}                    
                    />
                </td>
              </tr>
              <tr>
                <td>
                  <label htmlFor="last-name">
                    Last Name*:
                  </label>
                </td>
                <td>
                  <input
                    type="text"
                    className="form-component"
                    pattern="[A-Za-zА-Яа-яЁё]{3,20}"
                    title="[A-Za-zА-Яа-яЁё]{3,20}"
                    required="true"
                    ref={(input) => {this.inputRegistrationLastName = input}}
                    />
                </td>
              </tr>
              <tr>
                <td>
                  <label htmlFor="mailing-address1">Address*:</label>
                </td>
                <td>
                  <textarea
                    type="text"
                    className="form-component"
                    pattern="[A-Za-zА-Яа-яЁё]{10,250}"
                    title="[A-Za-zА-Яа-яЁё]{10,250}"
                    required="true"
                    ref={(input) => {this.inputRegistrationAdress = input}}
                    />
                </td>
              </tr>
              <tr>
                <td>
                  <label htmlFor="phone">Phone*:</label>
                </td>
                <td>
                  <input
                    type="text"
                    className="form-component"
                    name="phone"
                    pattern="[0-9]{7}"
                    title="[0-9]{7}"
                    required="true"
                    ref={(input) => {this.inputRegistrationPhone = input}}
                    />
                </td>
              </tr>
              <tr>
                <td>
                  <label htmlFor="email">Email*:</label>
                </td>
                <td>
                  <input type="text" className="form-component" name="email"
                   ref={(input) => {this.inputRegistrationEmail = input}}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <label htmlFor="password">Password*:</label>
                </td>
                <td>
                  <input type="password" className="form-component" name="password"
                   ref={(input) => {this.inputRegistrationPassword = input}}
                   />
                </td>
              </tr>
              <tr>
                <td>
                  <label htmlFor="passwordConfirm">
                    Password confirm*:
                  </label>
                </td>
                <td>
                  <input type="password" className="form-component" name="passwordConfirm"
                  ref={(input) => {this.inputRegistrationPasswordConfirm = input}}
                  />
                </td>
                <td>
                  <input type="role" className="form-component" name="role"
                  ref={(input) => {this.inputRegistrationRole = input}}
                  />

                  <SelectUserRole onValue={this.onValue.bind(this)} />
                </td>
              </tr>
              <tr>
                <td></td>
              </tr>
            </tbody>
          </table>
          <input type="submit" onClick={this.onSent.bind(this)} className="button"  defaultValue="Registry"/>
        {/*</form>*/}

      </div>
    );
  }
}


export default connect(
  state =>({
    user: state.user
  }),
  userActionDispatcher

)(RegistrationView);


