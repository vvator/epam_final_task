import React, {Component} from 'react';
import {connect} from 'react-redux';
import PassvordChange from './change-password';
import AccessDeniedView from '../../location-access-denied';
import userActionDispatcher from '../../../../actions/user-action';
import isAdminAccount from '../../../../../image/admin/isAdminAccount.png';

class AccountView extends Component {
  render() {
    console.log('REPAINT ', JSON.stringify(this.props.user))
    if (JSON.stringify(this.props.user) === JSON.stringify({}) || undefined) {
      return <AccessDeniedView/>
    }

    let imgStatusUser = (this.props.user.role === 'ADMIN')
      ? <img src={isAdminAccount} role="presentation" />
      : null;

    return <div className="AccountView">
      <h1>---{this.props.user.aa}---</h1>

      <PassvordChange/>

      <div>
        <h1>Account</h1>
        <div className="photo-pnl">
          <div className="account photo"></div>
          <a className="load-photo button">change photo</a>
          <a className="edit-account button">edit account</a>
        </div>

        {imgStatusUser}

        <table className="account-form-table">
          <tbody>
            <tr>
              <td>First Name:</td>
              <td>{this.props.user.firstName}--</td>
            </tr>
            <tr>
              <td>Last Name:</td>
              <td>{this.props.user.lastName}</td>
            </tr>
            <tr>
              <td>Address:</td>
              <td>{this.props.user.adress}</td>
            </tr>
            <tr>
              <td>Phone:</td>
              <td>{this.props.user.phone}</td>
            </tr>
            <tr>
              <td>Email:</td>
              <td>
                <a href={"mailto:" + this.props.user.email + "?subject=Hello"}>{this.props.user.email}</a>
              </td>
            </tr>
            <tr>
              <td>Password:</td>
              <td>
                <a className="change-pass button">change</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>;

  }
}

export default connect(state => ({user: state.user}), userActionDispatcher)(AccountView);
