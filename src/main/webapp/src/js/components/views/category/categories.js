import React, {Component} from 'react';

import CategoryListSquare from './category-list-square/category-list-square';

import CategoryAdder from './category-adder';

class CategoriesView extends Component {

  render() {

    return (
      <div className="ProductView">
        <h3>Category VIEW</h3>
        <CategoryAdder/>
        <hr/>

        <hr/>
        <CategoryListSquare/>

      </div>
    );
  }
}

export default CategoriesView;
