import React, { Component } from 'react';
import {connect} from 'react-redux';

import categoryActionDispatcher from '../../../actions/category-actions';

class CategoryAdder extends Component {
   state = {
        category: {
            imgUrls: []
        },
        categories: []
    };

    clearAllFromLocal() {
        this.setState({categories: []});
    };
    
    onDeleteProductFromLocalClick(category) {
        let categories = this
            .state
            .categories
            .filter(itemProduct => {
                return itemProduct.id !== category.id
            });
        this.setState({categories: categories});
    }

    addStateProductClick() {
        let add_category = this.filCategoryFromInputData();
        let findProductInCart = this
            .state
            .categories
            .filter(category => category.name === this.filCategoryFromInputData().name);
        if (findProductInCart.length === 0) {
          //  add_category;
         // let categories = 
            this.setState({
                categories: [
                    ...this
                        .state
                        .categories
                        .filter(category => category.name !== this.filCategoryFromInputData().name),
                    Object.assign({}, add_category)
                ]
            })
        } else {
           // findProductInCart[0].countInCart++;

            add_category = findProductInCart[0];
            this.setState({
                categories: [...this.state.categories]
            })
        }
    }

    addImgToProductClick(_category_) {
        let add_category = this.filCategoryFromInputData();
        console.log('++++++++++++++++++++++++++++++++++++++++++++', _category_);
        console.log('++++++++++++++++++++++++++++++++++++++++++++', add_category.imgUrls);
        Array
            .prototype
            .push
            .apply(_category_, add_category.imgUrls);
        console.log('++++++++++++++++++++++++++++++++++++++++++++', _category_);
        this.setState({
            ...this.state.categories
        })

    }

    /////////////////////////////////////////////////////////////////////////

    filCategoryFromInputData() {
      
        let inputName = this.inputName.value;
        let inputDescription = this.inputDescription.value;      
        let inputImgUrl = this.inputImgUrls.value;

        if (inputName === '') {
            return;
        }

        let category = {
            id: this.genUniqID(),
            name: inputName,
            description: inputDescription,

            imgUrls: [
                {
                    id: this.genUniqID(),
                    url: inputImgUrl
                }
            ]
        };
        console.log('filCategoryFromInputData:', category);
        return category;
    }

    addStateCategoriesClick() {
        console.log('addStateCategoriesClick:', this.state.categories);
        this
            .props
            .onAddCategories(this.state.categories);
        this.inputName.value = '';
    };

    onClickAddCategory(){
        console.log('onClickAddCategory:', this.filCategoryFromInputData());
        this
            .props
            .onAddCategories([this.filCategoryFromInputData()]);      
        this.inputName.value = '';
    };

  genUniqID(){
    return Date.now().toString();
}


    render() {

        let styleMinPic ={
            display: 'inline-block',
            height:'50px',
            background:'#CDCDCD',
            margin:'3px'
        }


        const styleNewAdd = {
            background: 'gray'
        }

        let notSend = [];
        for (let category of this.state.categories) {
            let imagesProduct = category.imgUrls;

            let imagesP = [];
            for (let img of imagesProduct) {
                imagesP.push(
                    <div key={this.genUniqID()+img.id} style={styleMinPic}>
                    <a>
                        <img src={img.url} height="50px;" role="presentation"/>
                    </a>
                    <div> x </div>
                    </div>

                );
            }
            notSend.push(
                <div key={this.genUniqID()+category.id}>
                    {category.name}| {category.category}| {imagesP}
                    <input type="text" ref={(input) => {this.inputImgUrl = input}}
                        default="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQa6wYcqvV70VdmsR-GZdl51GX3ZfNESy1pQXyLt2xtpi-1CiwDVA"/>
                    <button onClick={this.addImgToProductClick.bind(this, category.imgUrls)}>+img+</button>
                    <button onClick={this.onDeleteProductFromLocalClick.bind(this, category)}>x</button>
                </div>
            )
        }

        return (

            <div>

                <br/>
                <hr/> {/*id: */}
                name:
                <input type="text" ref={(input) => {this.inputName = input}}/>
                description:
                <input type="text" ref={(input) => {this.inputDescription = input}}/>
                imgUrl:
                <input type="text" ref={(input) => {this.inputImgUrls = input}} default="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQa6wYcqvV70VdmsR-GZdl51GX3ZfNESy1pQXyLt2xtpi-1CiwDVA"/>
                <button onClick={this.addStateProductClick.bind(this)}>+saveOnPage</button>

                <hr/>
                <button onClick={this.onClickAddCategory.bind(this)}>+</button>
                <hr/>
                <div style={styleNewAdd}>
                    {notSend}
                    <button onClick={this.clearAllFromLocal.bind(this)}>+clear All +</button>
                    <button onClick={this.addStateCategoriesClick.bind(this)}>+save All from local To BD+</button>
                </div>
                <hr/>

            </div>
        );
    }
}


export default connect(
    state => ({
    categories: state.categories,
  /*
        .filter(category => category.name.includes(state.filterCategories.name))
        .filter(category => category.category.includes(state.filterCategories.category))
        .filter(category => category.price.includes(state.filterCategories.price))
        .filter(category => category.stocked.includes(state.filterCategories.stocked))*/
        //,
    filterCategories: state.filterCategories
}), 
categoryActionDispatcher)(CategoryAdder);