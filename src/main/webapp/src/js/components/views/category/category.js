import React, {Component} from 'react';
import {connect} from 'react-redux';
import categoryActionDispatcher from '../../../actions/category-actions';

/*import { Link } from 'react-router';*/

import CategoryAdder from './category-adder';

class CategoryView extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }

  state = {
    category: {}
  };

  componentWillMount() {
    this
      .props
      .onLoadCategory(this.props.params.id);
  }

  render() {

  let arr = [];
  Array.prototype.push.apply(arr,this.props.category.imgUrls);
  let img  = <img src={arr[0].url} role="presentation"/>;
    return (
      <div className="CategoryView">
        CATEGORY VIEW
        <hr/> {this.props.category.id}
        <hr/> {this.props.category.name}
        <hr/> {img}
        <hr/> {this.props.category.description}
        <hr/>
        example product this.category:
        <hr/>
        <CategoryAdder/> {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  products: state.products,
  category: [...state.categories].find(category => {
    // console.log(ownProps.params)
  /*  if (category.id === Number(ownProps.params.id)) {
      console.log(category.id === Number(ownProps.params.id));
    }*/
    return category.id === Number(ownProps.params.id)

  }),
  ownProps
})
export default connect(mapStateToProps, categoryActionDispatcher)(CategoryView);