import React, {Component} from 'react';
import {connect} from 'react-redux';
import RecordCategory from '../recordCategory';
import categoryActionDispatcher from '../../../../actions/category-actions';

class CategoryListSquare extends Component {

    componentWillMount() {
        this
            .props
            .onLoadCategories();
    }

    render() {
        return (
            <div className="content">
                <RecordCategory/>
            </div>
        );
    }
}

export default connect(state => ({categories: state.categories}), categoryActionDispatcher)(CategoryListSquare);