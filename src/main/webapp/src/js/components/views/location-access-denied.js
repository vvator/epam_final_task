import React, {Component} from 'react'
import srcImgAccessDenied from '../../../image/access_denied.png';
import SigninView from '../../components/views/user/signin/signin';
class AccessDeniedView extends Component {
    render() {
        return (
            <div>
                <h1>Access Denied</h1>
                <marquee>Access Denied</marquee>
                <img src={srcImgAccessDenied} role="presentation"/>

                <SigninView/>

            </div>            
        )
    }
}

export default AccessDeniedView