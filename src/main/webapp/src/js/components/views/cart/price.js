import React, {Component} from 'react'
import {connect} from 'react-redux';

class PriceCartComponent extends Component {
    totalCost() {
        let cost = 0;
        /*let cartElements = []
        Array.prototype.push.apply(cartElements,this.props.cart);
         for(let cartElement of cartElements){
             cost += Number(cartElement.product.price) * Number(cartElement.countInCart);
             console.log(cartElement.product.price)
         }*/
         this.props.cart.forEach((cartElement) => cost += Number(cartElement.product.price) * Number(cartElement.countInCart));
        return cost;
    }
    render() {
        return (
            <div> 
                {this.totalCost()}$
            </div>
        )
    }
}

export default connect(state => ({cart: state.cart}), dispatch => ({}))(PriceCartComponent);