import React, {Component} from 'react';
import {connect} from 'react-redux';
import PriceCartComponent from '../../views/cart/price';
import CartRecords from './cartRecords';

import cartActionDispatcher from '../../../actions/cart-actions';
import ButtonLoginView from '../../buttons/btn-view-login';
import '../../../../styles/cart.css';
import srcCartEmpty from '../../../../image/cart_empty.png';

class CartView extends Component {
  onClickButtonBUY() {
    this 
      .props
      .onBUY(this.props.cart);
  }

  render() {
    return (
      <div className="content-wrapper">
      <div className="CartView">
        <h3>CART</h3>
        <br/>
        
        <table className="cart product-table">
          <thead>
            <tr>
              <td className="col-remove"></td>
              <td className="col-photo"></td>
              <td className="col-description"></td>
              <td className="col-count"></td>
            </tr>
          </thead>
          <CartRecords cart={this.props.cart}/>

        </table>
        
        <img src={srcCartEmpty} height="100px" role="presentation"/>
        

        <div className="cart right-cart">
          <div className="confirm">
            <div className="total">
              Total:<PriceCartComponent/>
            </div> 
            <input type="checkbox" value="aaa"/>payment on delivery
            <div className="buttons">


              {(JSON.stringify(this.props.user)===JSON.stringify({}))
                      ?             
                    <ButtonLoginView/>
                      :
                      <button
                      onClick={this
                      .onClickButtonBUY
                      .bind(this)}
                      className="button btn-green">BUY</button>
              }

              <button
                onClick={this
                .props
                .onCartClear
                .bind(this)}
                className="button btn-green">clear</button>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default connect(state => ({cart: state.cart, user: state.user}), cartActionDispatcher)(CartView);