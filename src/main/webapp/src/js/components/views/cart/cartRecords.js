import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import cartActionDispatcher from '../../../actions/cart-actions';

import noImage from '../../../../image/noImage.png';

class CartRecords extends Component {
  onChangeCount(product, e) {
    let inputC = e.target.value;
    console.log(inputC);
    this
      .props
      .onChangeCountToCart(product, inputC);
  }

  render() {

    let cartRecords = [];
    this
      .props
      .cart
      .map((cartElement, index) => {

        let imgMain = noImage;
        if (cartElement.product.imgUrls.length > 0) {
          if (!cartElement.product.imgUrls[0].url) {
            imgMain = (cartElement.product.imgUrls.length !== 0)
              ? cartElement.product.imgUrls[0].url
              : noImage;
          } else {
            imgMain = cartElement.product.imgUrls[0].url;
          }
        }
        cartRecords.push(
          <tr key={index}>
            <td>             
              <Link
                onClick={this
                .props
                .onCartDelete
                .bind(this, cartElement)}
                className="remove">Удалить</Link>
            </td>
            <td>
              <a href="#">
                <img src={imgMain} className="product-img-pnl" role="presentation"/>
              </a>
            </td>
            <td>
              <div className="title">
                <Link to={"/product/" + cartElement.product.id}>{cartElement.product.name}</Link>
              </div>
              <div className="desc">description
              </div>
            </td>
            <td>
              <div className="q">
                <Link
                  onClick={this
                  .props
                  .onMinusToCart
                  .bind(this, cartElement.product)}
                  className="minus">-</Link>
                <input
                  type="text"
                  onChange={this
                  .onChangeCount
                  .bind(this, cartElement)}
                  name="quantity"
                  className="inputbox q_input"
                  value={cartElement.countInCart}/>

                <Link
                  onClick={this
                  .props
                  .onAddToCart
                  .bind(this, cartElement.product)}
                  className="plus">+</Link>

              </div>
              <div className="price">× {cartElement.product.price}$</div>
              <div className="total">= {cartElement.product.price * cartElement.countInCart}$</div>
            </td>
          </tr>
        )
        return cartElement;
      })
    return (
      <tbody>
        {cartRecords}
      </tbody>
    );
  }
}

export default connect(state => ({cart: state.cart}), cartActionDispatcher)(CartRecords);