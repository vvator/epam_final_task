import React, {Component} from 'react';
import {connect} from 'react-redux';
import PriceCartComponent from '../../../views/cart/price';
import OrderRecords from '../orders/orderRecords';

import orderActionDispatcher from '../../../../actions/order-actions';
import ButtonLoginView from '../../../buttons/btn-view-login';


class OrdersView extends Component {

     componentWillMount() {      
        this.props.onLoadOrders();
    }
  onClickButtonBUY() {
    this 
      .props
      .onBUY(this.props.order);
  }

  render() {
    return (
        
      <div className="content-wrapper">  
      <div className="OrdersView">
        <h3>ORDERS HISTORY</h3>
        <table className="cart product-table">
          <thead>
            <tr>
              <td className="col-remove"></td>
              <td className="col-photo"></td>
              <td className="col-description"></td>
              <td className="col-count"></td>
            </tr>
          </thead>
          <OrderRecords order={this.props.order}/>

        </table>

        
      </div>
      </div>
    );
  }
}

export default connect(state => ({order: state.order, user: state.user}), orderActionDispatcher)(OrdersView);