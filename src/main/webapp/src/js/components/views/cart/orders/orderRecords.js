import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import orderActionDispatcher from '../../../../actions/order-actions';
import SelectOrderStatus from '../../product/select-list-orderStatus';
import noImage from '../../../../../image/noImage.png';

class OrderRecords extends Component {


    jsonSerializeToObj(json){
        return JSON.parse(json);
    }




aaaaa(_cart_){
        let cartRecords = [];

  console.log('---');
  console.log(_cart_);

let cart = [];
Array.prototype.push.apply(cart,_cart_);

      cart
      .map((cartElement, index) => {

        let imgMain = noImage;
        if (cartElement.product.imgUrls.length > 0) {
          if (!cartElement.product.imgUrls[0].url) {
            imgMain = (cartElement.product.imgUrls.length !== 0)
              ? cartElement.product.imgUrls[0].url
              : noImage;
          } else {
            imgMain = cartElement.product.imgUrls[0].url;
          }
        }
        cartRecords.push(
          <tr key={index}>
            <td>
             {/* <a href="#">*/}
                <img src={imgMain} height="50px"  role="presentation"/>
            { /* </a>*/}
            </td>
            <td>
              <div className="title">
                <Link to={"/product/" + cartElement.product.id}>{cartElement.product.name}</Link>
              </div>
              <div className="desc">description
              </div>
            </td>
            <td>
              <div className="q">               

              </div>
              <div className="price">count:{cartElement.countInCart} ×  cost {cartElement.product.price}$</div>
              <div className="total">= {cartElement.product.price * cartElement.countInCart}$</div>
            </td>
          </tr>
        )
        return cartElement;
      })
       return cartRecords
}



onValue(order,e){
    console.log(e.target.value + ' --- '+order.id);
    order.status = e.target.value;

    this.props.setOrderStatus(order);
    return e.target.value   
}
    render() {

const styleOrderRecord = {
    background:"white",
    wigth: "100%"
}


    let orderRecords = [];
    this
      .props
      .order
      .map((order, index) => {
        let cart =  this.jsonSerializeToObj(order.snapShootJSON);
        let products = this.aaaaa(cart);
        console.log(products);
        /*for(let product of cart){

        products.push(product.id);
        }*/
        orderRecords.push(            
          <tr key={index}>
            <td>               
                <div style={styleOrderRecord}>
                  ORDER # {order.id} 
                  status:{order.status}
                  {products}
                </div>
                <SelectOrderStatus onValue={this.onValue.bind(this,order)} />
              <hr/>
            </td>
          </tr>
        )        
        return order;

      })
    return (
      <tbody>
        {orderRecords}
      </tbody>
    );
  }
}

export default connect(state => ({order: state.order}), orderActionDispatcher)(OrderRecords);