import React, {Component} from 'react';
import SideMenu from '../../menus/side-menu/side-menu';

class RightSidebar extends Component {
    render() {
        return (
            <div className="right-sidebar">

                <SideMenu/>

                <div className="side-menu">
                    <div className="side-menu-header">Product Сategory</div>
                    <div className="side-menu-body">
                        <ul>
                            <li>
                                <a href="#">Camera (2)</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="side-menu">
                    <div className="side-menu-header">Labels</div>
                    <div className="side-menu-body">
                        <ul>
                            <li>
                                <a href="#">Camera (2)</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        );
    }
}

export default RightSidebar;