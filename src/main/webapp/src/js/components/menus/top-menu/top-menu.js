import React, {Component} from 'react';
import {Link} from 'react-router';
import SearchComponent from '../../search';

class TopMenu extends Component {

    render() {
        return (
            <div className="top-menu">
                <ul>
                    <li>
                        <Link to='/home'>Home</Link>
                    </li>
                    <li>
                        <Link to='/cart'>CartView</Link>
                    </li>
                    <li>
                        <Link to='/account'>Account</Link>
                    </li>
                    <li>
                        <Link to='/registration'>Registration</Link>
                    </li>
                    <li>
                        <Link to='/signin'>Log In</Link>
                    </li>
                </ul>

                
                <SearchComponent/>



               


            </div>
        );
    }
}

export default TopMenu;