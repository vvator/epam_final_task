import React, {Component} from 'react';
import {connect} from 'react-redux';
import settingsActionDispatcher from '../../../actions/setting-action';
class ItemTypeView extends Component {
    render () {
        return (            
            <div className="view-pnl">
                <a id="grid-view" className="type-view-btn" onClick={this.props.onSetView.bind(this,'GRID')}>Grid</a>
                <a id="list-view" className="type-view-btn" onClick={this.props.onSetView.bind(this,'DETAILS')}>Details</a>
                {this.props.settings.view}
            </div>           
        )
    }
}

export default connect(
    state =>({
        settings: state.settings 
    }),
   settingsActionDispatcher
)(ItemTypeView) 