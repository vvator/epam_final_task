import React, {Component} from 'react';
import {Link} from 'react-router'

import PriceCartComponent from '../../views/cart/price';
import ButtonLogoutView from '../../../components/buttons/btn-logout';
import ViewComponent from '../../../components/views/cart/view';


class MainMenu extends Component {

    render() {
        return (
            <div className='main-menu'>

                <ul>
                    <li>
                        <Link to='/home'>Home</Link>
                    </li>
                    <li>
                        <Link to='/orders'>Orders</Link>
                    </li>             
                    <li>
                        <Link to='/products'>Products</Link>
                    </li>
                    <li>
                        <Link to='/categories'>Category</Link>
                    </li>
                    <li>
                        <Link to='/account'>Account</Link>
                    </li>
                    <li>
                        <Link to='/registration'>Registration</Link>
                    </li>
                    <li>
                        <Link to='/signin'>SignIn</Link>
                    </li>
                    <li>
                        <ButtonLogoutView />
                    </li>
                </ul>

                <div className='main-menu-cart'>
                    <div className='mycart'>
                        <Link to='/cart' className='e'>CartView</Link>
                        <ViewComponent/>
                        <PriceCartComponent/>
                    </div>
                </div>
            </div>
        );
    }
}

export default MainMenu;