import React, {Component} from 'react';
import {Link} from 'react-router'

import {connect} from 'react-redux';

class SideMenu extends Component {

    render() {
        //   console.log(ownProps.params.id)
        let rows = [];

     //   console.log(JSON.stringify(this.props.categories));
        this
            .props
            .categories
            .forEach((category) => {
                rows.push(
                    <li key={category.id}>
                        {/* <a href="#">{category.name} (2) </a>*/}
                        <Link to={"/category/" + category.id}>{category.name}
                            (2)</Link>
                    </li>
                )
            })
        return (
            <div className="side-menu">
                <div className="side-menu-header">
                    Product Сategory
                </div>
                <div className="side-menu-body">
                    <ul>
                        {rows}
                        {/*<li>
                            <a href="#">Camera (2)
                            </a>
                        </li>
                        <li>
                            <a href="#">Computers (1)
                            </a>
                        </li>
                        <li>
                            <a href="#">Electronic (2)
                            </a>
                        </li>
                        <li>
                            <a href="#">Featured (10)
                            </a>
                        </li>
                        <li>
                            <a href="#">Gadget (2)
                            </a>
                        </li>
                        <li>
                            <a href="#">Parfume (1)
                            </a>
                        </li>
                        <li>
                            <a href="#">Phone (1)
                            </a>
                        </li>
                        <li>
                            <a href="#">Shoes (2)
                            </a>
                        </li>*/}
                    </ul>
                </div>

            </div>

        );
    }

}

export default connect((state, ownProps) => ({
    categories: state.categories, ownProps
    /*  .filter(product => product.name.includes(state.filterProducts.name))
    .filter(product => product.category.includes(state.filterProducts.category))
    .filter(product => product.price.includes(state.filterProducts.price))
  .filter(product => product.stocked.includes(state.filterProducts.stocked))*/
    /* ,
    filterProducts: state.filterProducts*/
}),
// cartActionDispatcher
)(SideMenu);
