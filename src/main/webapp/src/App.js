import React, {Component} from 'react';
//import facebook from './facebook.png';
import './App.css';


import TopMenu from './js/components/menus/top-menu/top-menu';
import MainMenu from './js/components/menus/main-menu/main-menu';
// import RecordProduct from './js/components/views/product/recordProduct';
// import productActionDispatcher from './js/actions/product-actions';

import {connect} from 'react-redux';
import userActionDispatcher from './js/actions/user-action';
import RotatorImage from './js/components/views/product/gallery/rotator';


//import './styles/default/main-menu.css';
//import './styles/screen-lt-480/main-menu.css';
/*import ItemTypeView from './js/components/menus/item-type-view/item-type-view';
import ArrowsPreviewNextView from './js/components/buttons/arrows-previos-next';
import RightSidebar from './js/components/views/sidebar-right/sidebar-right';
import View from './js/components/views/View';*/

class App extends Component {

  state = ({
    product: {
      imgUrls: []
    },
    products: []
  });

  clearAllFromLocal() {
    this.setState({products: []});
  }
  onDeleteProductFromLocalClick(product) {
    let products = this
      .state
      .products
      .filter(itemProduct => {
        return itemProduct.id !== product.id
      });

    this.setState({products: products});
  }

  /////////////////////////////////////////////////////////////////////////

  addFilterClick() {
    let inputId = this.inputFilterId.value;
    let inputName = this.inputFilterName.value;
    let inputCategory = this.inputFilterCategory.value;
    let inputPrice = this.inputFilterPrice.value;
    let inputStocked = this.inputFilterStocked.value;
    let inputImgUrl = this.inputFilterImgUrl.value;

    console.log('onFilterProduct:', inputId, inputName, inputCategory, inputPrice, inputStocked, inputImgUrl);
    this
      .props
      .onFilterProduct(inputId, inputName, inputCategory, inputPrice, inputStocked, inputImgUrl);
  }

  render() {

    return (

      <div className="App">

        <hr/>
        
        
        

        <RotatorImage/>

        <br/>
        <hr/>

        <hr/>











<div className="wrapper">
<div className="top-menu">
  <TopMenu/>
</div>
<header>
  <div className="logo">
    <img src="images/logo.png" alt="logo" role="presentation"/>
    <p className="subtitle"> THE BEST SHOP IN INTERNET</p>
  </div>
  <div className="banner"></div>
  <div className="main-menu">
    <MainMenu/>
  </div>
</header>

{this.props.children}
{/*<div className="content-wrapper">
                <ItemTypeView/>
                <ArrowsPreviewNextView/>    
                <div className="left-sidebar">
                    <div className="content">
                    content
                    {this.props.children}
                    </div>
                    
                    <div className="pagination">
                    <div id="link-next-product">
                        <a href="#">Next Product »</a>
                    </div>
                    <div id="link-home"><a id="index.html">Home</a></div>
                    </div>   

                </div>
                <RightSidebar/>
</div>
*/}



  <footer>
      <div id="content-footer">
          <p id="copyright">Copyright 2011 <a href="#">Blogger Store v.2</a></p>                   
          <p id="info">POWERED BY BLOGGER. DESIGN BY 
            <a href="#">JAVA TEMPLATES</a>
            <a href="#"> - BLOGGER TEMPLATES 2012</a>
          </p>
          <div id="pay-system">
              <a href="#"><img src="images/checkout_google.png" role="presentation"/></a>
              <a href="#"><img src="images/checkout_paypal.png" role="presentation"/></a>
          </div>
      </div>
  </footer>
</div>











           
          
      </div>
    );
  }
}

export default connect(state => ({
  products: state.products, 
  filterProducts: state.filterProducts
}), userActionDispatcher)(App);
