import React, {Component} from 'react';
//import facebook from './facebook.png';
import './App.css';
import SideMenu from './js/components/menus/side-menu/side-menu';

import TopMenu from './js/components/menus/top-menu/top-menu';
import MainMenu from './js/components/menus/main-menu/main-menu';
// import RecordProduct from './js/components/views/product/recordProduct';
// import productActionDispatcher from './js/actions/product-actions';

import {connect} from 'react-redux';
import userActionDispatcher from './js/actions/user-action';



import RotatorImage from './js/components/views/product/gallery/rotator';

class App extends Component {

  state = {
    product: {
      imgUrls: []
    },
    products: []
  };

  clearAllFromLocal() {
    this.setState({products: []});
  }
  onDeleteProductFromLocalClick(product) {
    let products = this
      .state
      .products
      .filter(itemProduct => {
        return itemProduct.id !== product.id
      });

    this.setState({products: products});
  }

  /////////////////////////////////////////////////////////////////////////

  addFilterClick() {
    let inputId = this.inputFilterId.value;
    let inputName = this.inputFilterName.value;
    let inputCategory = this.inputFilterCategory.value;
    let inputPrice = this.inputFilterPrice.value;
    let inputStocked = this.inputFilterStocked.value;
    let inputImgUrl = this.inputFilterImgUrl.value;

    console.log('onFilterProduct:', inputId, inputName, inputCategory, inputPrice, inputStocked, inputImgUrl);
    this
      .props
      .onFilterProduct(inputId, inputName, inputCategory, inputPrice, inputStocked, inputImgUrl);
  }

  render() {

    return (

      <div className="App">

        <hr/>
        <TopMenu/>
        <MainMenu/>
        <SideMenu/>

        <RotatorImage/>

        <br/>
        <hr/> {this.props.children}

        <hr/>

      </div>
    );
  }
}

/*
(state, ownProps) => ({
  tracks: state.tracks.filter(track => track.name.includes(state.filterTracks)),
  ownProps
}),*/

export default connect(state => ({
  products: state.products,
  /*
    .filter(product => product.name.includes(state.filterProducts.name))
    .filter(product => product.category.includes(state.filterProducts.category))
    .filter(product => product.price.includes(state.filterProducts.price))
    .filter(product => product.stocked.includes(state.filterProducts.stocked))    */
  filterProducts: state.filterProducts
}), userActionDispatcher)(App);
