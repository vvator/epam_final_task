package by.epamlab.dao;

import by.epamlab.exceptions.DAOExceptionSOAP;

public interface IProductDao {

    String getPrice(long id) throws DAOExceptionSOAP;

}
