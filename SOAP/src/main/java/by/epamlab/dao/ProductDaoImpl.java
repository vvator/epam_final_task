package by.epamlab.dao;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import by.epamlab.exceptions.DAOExceptionSOAP;
import by.epamlab.model.Product;

@Repository
public class ProductDaoImpl implements IProductDao {

    private static final Logger LOG = Logger.getLogger(ProductDaoImpl.class);

    @Qualifier
    @Autowired(required = true)
    private SessionFactory sessionFactory;

    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public String getPrice(final long id) throws DAOExceptionSOAP {
        LOG.debug("invoke SOAP DAO getPrice(" + id + ")");
        Session session = null;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Product findProduct = session.get(Product.class, id);
        session.close();
        if (findProduct != null) {
            LOG.debug("Product getByID(" + id + "): " + findProduct);
            return findProduct.getPrice();
        } else {
            throw new DAOExceptionSOAP("NOT FOUND PRICE");
        }
    }

}
