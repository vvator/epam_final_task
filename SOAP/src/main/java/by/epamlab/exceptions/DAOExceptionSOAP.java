package by.epamlab.exceptions;

public class DAOExceptionSOAP extends Exception {
    private static final long serialVersionUID = 1L;
    private String message;

    public DAOExceptionSOAP() {
    }

    public DAOExceptionSOAP(final String message) {
        super(message);
        this.message = message;
    }

    public DAOExceptionSOAP(final Throwable cause) {
        super(cause);
        this.message = cause.toString();
    }

    public DAOExceptionSOAP(final String message, final Throwable cause) {
        super(message, cause);
    }

    @Override
    public String toString() {
        return "NotFoundPriceExceptionSOAP :" + message;
    }
}
