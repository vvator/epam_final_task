package by.epamlab.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epamlab.dao.ProductDaoImpl;
import by.epamlab.exceptions.DAOExceptionSOAP;

@Service
@Transactional
public class ProductServiceImpl implements IProductService {
    private ProductDaoImpl dao;

    public void setDao(final ProductDaoImpl dao) {
        this.dao = dao;
    }

    @Override
    public String getPrice(final long id) throws DAOExceptionSOAP {
        return dao.getPrice(id);
    }

}
