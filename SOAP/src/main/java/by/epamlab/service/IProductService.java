package by.epamlab.service;

import by.epamlab.exceptions.DAOExceptionSOAP;

public interface IProductService {

    String getPrice(long id) throws DAOExceptionSOAP;

}
