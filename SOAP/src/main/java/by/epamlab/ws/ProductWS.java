package by.epamlab.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import by.epamlab.dao.ProductDaoImpl;
import by.epamlab.exceptions.DAOExceptionSOAP;

@WebService
@SOAPBinding(style = Style.RPC)
@SpringBootApplication(scanBasePackages = "by")
public class ProductWS {
	@Qualifier
	@Autowired(required = true)
	private ProductDaoImpl productDao;

	@WebMethod(exclude = true)
	public void setProductDao(final ProductDaoImpl productDao) {
		this.productDao = productDao;
	}

	@WebMethod(operationName = "getPriceById")
	public String getHelloWorld(@WebParam(name = "id") final Long id) throws DAOExceptionSOAP {
		return productDao.getPrice(id);
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ProductWS.class);
		app.run(args);
	}

}
